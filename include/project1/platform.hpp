#pragma once

#include <sdkddkver.h>

#define WIN32_LEAN_AND_MEAN
#include <d3d11.h>
#include <d3dcompiler.h>
#include <dxgi.h>
#include <shellapi.h>
#include <windows.h>
