#pragma once

#include <vector>

#include "project1/input/event.hpp"

namespace project1::input {
struct Axis
{
public:
  struct Binding
  {
  public:
    std::uint16_t number;
    float scale;
    Event::Type type;
    float value = 0.0f;

    constexpr Binding() noexcept = delete;

    constexpr Binding(std::uint16_t number, float scale, Event::Type type) noexcept
      : number(number)
      , scale(scale)
      , type(type)
    {}

    constexpr Binding(Binding const&) noexcept = default;

    constexpr Binding(Binding&&) = default;

    constexpr virtual ~Binding() noexcept = default;

    constexpr auto operator=(Binding const&) & noexcept -> Binding& = default;

    constexpr auto operator=(Binding&&) & noexcept -> Binding& = default;

  protected:
  private:
  };

  constexpr Axis() noexcept = delete;

  Axis(Axis const&) noexcept = delete;

  Axis(Axis&&) = default;

  virtual ~Axis() noexcept = default;

  auto operator=(Axis const&) & noexcept -> Axis& = delete;

  auto operator=(Axis&&) & noexcept -> Axis& = default;

  auto begin_update() & noexcept -> void;

  auto end_update() & noexcept -> void;

  [[nodiscard]] constexpr auto get() const& noexcept -> float { return m_value; }

  static auto new_(std::vector<Binding>&& bindings) noexcept -> Axis;

  static auto new_pointer(std::vector<Binding>&& bindings) noexcept -> Axis*;

  auto process_event(Event const& event) & noexcept -> void;

protected:
private:
  std::vector<Binding> m_bindings = std::vector<Binding>();
  float m_value = 0.0f;

  explicit Axis(std::vector<Binding>&& bindings) noexcept;
};
}
