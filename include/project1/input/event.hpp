#pragma once

#include <cstdint>

namespace project1::input {
struct Event
{
public:
  enum class Type : std::uint8_t
  {
    AbsoluteAxis = 0,
    Key = 1,
    RelativeAxis = 2,
  };

  std::uint16_t number;
  Type type;
  float value;

  constexpr Event() noexcept = delete;

  constexpr Event(std::uint16_t number, Type type, float value) noexcept
    : number(number)
    , type(type)
    , value(value)
  {}

  constexpr Event(Event const&) noexcept = default;

  constexpr Event(Event&&) noexcept = default;

  constexpr virtual ~Event() noexcept = default;

  constexpr auto operator=(Event const&) & noexcept -> Event& = default;

  constexpr auto operator=(Event&&) & noexcept -> Event& = default;

protected:
private:
};
}
