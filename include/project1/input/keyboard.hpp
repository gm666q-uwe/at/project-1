#pragma once

#include <string>
#include <string_view>

#include "project1/input/device.hpp"

namespace project1::input {
struct Keyboard final : public Device
{
public:
  // constexpr Keyboard() noexcept = delete;

  Keyboard(Keyboard const&) noexcept = delete;

  Keyboard(Keyboard&&) = default;

  ~Keyboard() noexcept override = default;

  auto operator=(Keyboard const&) & noexcept -> Keyboard& = delete;

  auto operator=(Keyboard&&) & noexcept -> Keyboard& = default;

  auto begin_update() & noexcept -> void override;

  [[nodiscard]] auto characters() const& noexcept -> std::wstring_view;

  auto end_update() & noexcept -> void override;

  static auto new_() noexcept -> Keyboard;

  static auto new_pointer() noexcept -> Keyboard*;

  auto process_char(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

  auto process_key(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

protected:
private:
  std::wstring m_characters = std::wstring();

  Keyboard() noexcept;
};
}
