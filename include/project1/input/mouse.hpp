#pragma once

#include "project1/input/device.hpp"

namespace project1::input {
struct Mouse final : public Device
{
public:
  // constexpr Mouse() noexcept = delete;

  Mouse(Mouse const&) noexcept = delete;

  Mouse(Mouse&&) = default;

  ~Mouse() noexcept override = default;

  auto operator=(Mouse const&) & noexcept -> Mouse& = delete;

  auto operator=(Mouse&&) & noexcept -> Mouse& = default;

  auto begin_update() & noexcept -> void override;

  auto end_update() & noexcept -> void override;

  static auto new_() noexcept -> Mouse;

  static auto new_pointer() noexcept -> Mouse*;

  auto process_left_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

  auto process_middle_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

  auto process_move(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

  auto process_right_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

  auto process_x_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void;

protected:
private:
  POINTS m_position;
  POINTS m_previous_position;

  Mouse() noexcept;
};
}
