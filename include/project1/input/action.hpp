#pragma once

#include <vector>

#include "project1/input/event.hpp"

namespace project1::input {
class Action
{
public:
  struct Binding
  {
  public:
    float dead_zone;
    std::uint16_t number;
    bool previous_value = false;
    Event::Type type;
    bool value = false;

    constexpr Binding() noexcept = delete;

    constexpr Binding(float dead_zone, std::uint16_t number, Event::Type type) noexcept
      : dead_zone(dead_zone)
      , number(number)
      , type(type)
    {}

    constexpr Binding(Binding const&) noexcept = default;

    constexpr Binding(Binding&&) = default;

    constexpr virtual ~Binding() noexcept = default;

    constexpr auto operator=(Binding const&) & noexcept -> Binding& = default;

    constexpr auto operator=(Binding&&) & noexcept -> Binding& = default;

  protected:
  private:
  };

  constexpr Action() noexcept = delete;

  Action(Action const&) noexcept = delete;

  Action(Action&&) = default;

  virtual ~Action() noexcept = default;

  auto operator=(Action const&) & noexcept -> Action& = delete;

  auto operator=(Action&&) & noexcept -> Action& = default;

  auto begin_update() & noexcept -> void;

  auto end_update() & noexcept -> void;

  [[nodiscard]] constexpr auto get() const& noexcept -> bool { return m_value; }

  [[nodiscard]] constexpr auto get_pressed() const& noexcept -> bool { return m_value && !m_previous_value; }

  [[nodiscard]] constexpr auto get_released() const& noexcept -> bool { return !m_value && m_previous_value; }

  static auto new_(std::vector<Binding>&& bindings) noexcept -> Action;

  static auto new_pointer(std::vector<Binding>&& bindings) noexcept -> Action*;

  auto process_event(Event const& event) & noexcept -> void;

protected:
private:
  std::vector<Binding> m_bindings = std::vector<Binding>();
  bool m_previous_value = false;
  bool m_value = false;

  explicit Action(std::vector<Binding>&& bindings) noexcept;
};
}
