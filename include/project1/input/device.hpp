#pragma once

#include <queue>

#include "project1/input/event.hpp"

namespace project1::input {
struct Device
{
public:
  enum class Type : std::uint8_t
  {
    Controller = 0,
    Keyboard = 1,
    Mouse = 2,
    Other = 3,
    Unknown = 4,
  };

  constexpr Device() noexcept = delete;

  Device(Device const&) noexcept = delete;

  Device(Device&&) = default;

  virtual ~Device() noexcept = default;

  auto operator=(Device const&) & noexcept -> Device& = delete;

  auto operator=(Device&&) & noexcept -> Device& = default;

  virtual auto begin_update() & noexcept -> void = 0;

  virtual auto end_update() & noexcept -> void = 0;

  [[nodiscard]] auto has_event() const& noexcept -> bool;

  auto pop_event() & noexcept -> Event;

  [[nodiscard]] constexpr auto type() const& noexcept -> Type { return m_type; }

protected:
  std::queue<Event> m_events = std::queue<Event>();
  Type m_type;

  explicit Device(Type type = Type::Unknown) noexcept;

private:
};
}
