#pragma once

#include "project1/ecs/utility/counter.hpp"
#include "project1/ecs/utility/handle_data_index.hpp"

namespace project1::ecs {
struct Handle
{
public:
  utility::Counter counter = utility::Counter(0);
  utility::HandleDataIndex handle_data_index = utility::HandleDataIndex(0);

  // constexpr Handle() noexcept = delete;

  Handle(Handle const&) noexcept = default;

  Handle(Handle&&) noexcept = default;

  constexpr virtual ~Handle() noexcept = default;

  auto operator=(Handle const&) & noexcept -> Handle& = default;

  auto operator=(Handle&&) & noexcept -> Handle& = default;

protected:
private:
};
}
