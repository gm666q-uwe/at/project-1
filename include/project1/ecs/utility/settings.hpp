#pragma once

#include <bitset>

#include <boost/mpl/contains.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/size.hpp>

namespace project1::ecs::utility {
template<typename C, typename S, typename T>
struct Settings
{
public:
  constexpr Settings() noexcept = delete;

  constexpr Settings(Settings const&) noexcept = delete;

  constexpr Settings(Settings&&) noexcept = delete;

  constexpr virtual ~Settings() noexcept = delete;

  constexpr auto operator=(Settings const&) & noexcept -> Settings& = delete;

  constexpr auto operator=(Settings&&) & noexcept -> Settings& = delete;

  template<typename U>
  static constexpr auto component_bit() noexcept -> std::size_t
  {
    return component_id<U>();
  }

  static constexpr auto component_count() noexcept -> std::size_t
  {
    using result = typename boost::mpl::size<C>;
    return result::value;
  }

  template<typename U>
  static constexpr auto component_id() noexcept -> std::size_t
  {
    using iter = typename boost::mpl::find<C, U>::type;
    return iter::pos::value;
  }

  template<typename U>
  static constexpr auto is_component() noexcept -> bool
  {
    using result = typename boost::mpl::contains<C, U>;
    return result::value;
  }

  template<typename U>
  static constexpr auto is_signature() noexcept -> bool
  {
    using result = typename boost::mpl::contains<S, U>;
    return result::value;
  }

  template<typename U>
  static constexpr auto is_tag() noexcept -> bool
  {
    using result = typename boost::mpl::contains<T, U>;
    return result::value;
  }

  static constexpr auto signature_count() noexcept -> std::size_t
  {
    using result = typename boost::mpl::size<S>;
    return result::value;
  }

  template<typename U>
  static constexpr auto signature_id() noexcept -> std::size_t
  {
    using iter = typename boost::mpl::find<S, U>::type;
    return iter::pos::value;
  }

  template<typename U>
  static constexpr auto tag_bit() noexcept -> std::size_t
  {
    return component_count() + tag_id<U>();
  }

  static constexpr auto tag_count() noexcept -> std::size_t
  {
    using result = typename boost::mpl::size<T>;
    return result::value;
  }

  template<typename U>
  static constexpr auto tag_id() noexcept -> std::size_t
  {
    using iter = typename boost::mpl::find<T, U>::type;
    return iter::pos::value;
  }

  using Bitset = std::bitset<component_count() + tag_count()>;

protected:
private:
};
}
