#pragma once

#include <cstddef>

#include <boost/serialization/strong_typedef.hpp>

namespace project1::ecs::utility {
BOOST_STRONG_TYPEDEF(std::ptrdiff_t, Counter)
}
