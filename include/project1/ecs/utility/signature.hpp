#pragma once

#include <boost/mpl/vector.hpp>

namespace project1::ecs::utility {
template<typename... T>
using Signature = boost::mpl::vector<T...>;
}
