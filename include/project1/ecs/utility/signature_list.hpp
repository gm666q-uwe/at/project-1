#pragma once

#include <boost/mpl/vector.hpp>

namespace project1::ecs::utility {
template<typename... T>
using SignatureList = boost::mpl::vector<T...>;
}
