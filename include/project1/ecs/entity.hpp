#pragma once

#include "project1/ecs/utility/data_index.hpp"
#include "project1/ecs/utility/handle_data_index.hpp"
#include "project1/ecs/utility/settings.hpp"

namespace project1::ecs {
template<typename T>
struct Entity
{
public:
  bool active = false;
  typename T::Bitset bitset;
  utility::DataIndex data_index = utility::DataIndex(0);
  utility::HandleDataIndex handle_data_index = utility::HandleDataIndex(0);

  // constexpr Entity() noexcept = delete;

  constexpr Entity(Entity const&) noexcept = default;

  constexpr Entity(Entity&&) noexcept = default;

  constexpr virtual ~Entity() noexcept = default;

  constexpr auto operator=(Entity const&) & noexcept -> Entity& = default;

  constexpr auto operator=(Entity&&) & noexcept -> Entity& = default;

protected:
private:
};
}
