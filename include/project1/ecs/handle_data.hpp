#pragma once

#include "project1/ecs/utility/counter.hpp"
#include "project1/ecs/utility/entity_index.hpp"

namespace project1::ecs {
struct HandleData
{
public:
  utility::Counter counter = utility::Counter(0);
  utility::EntityIndex entity_index = utility::EntityIndex(0);

  // constexpr HandleData() noexcept = delete;

  HandleData(HandleData const&) noexcept = default;

  HandleData(HandleData&&) noexcept = default;

  constexpr virtual ~HandleData() noexcept = default;

  auto operator=(HandleData const&) & noexcept -> HandleData& = default;

  auto operator=(HandleData&&) & noexcept -> HandleData& = default;

protected:
private:
};
}
