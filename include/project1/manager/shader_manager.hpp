#pragma once

#include <unordered_map>
#include <vector>

#include "project1/graphics/renderer.hpp"
#include "project1/utility/singleton.hpp"

namespace project1::manager {
class ShaderManager : public utility::Singleton<ShaderManager>
{
  friend utility::Singleton<ShaderManager>;

public:
  constexpr ShaderManager() noexcept = delete;

  constexpr ShaderManager(ShaderManager const&) noexcept = delete;

  constexpr ShaderManager(ShaderManager&&) = delete;

  constexpr auto operator=(ShaderManager const&) & noexcept -> ShaderManager& = delete;

  constexpr auto operator=(ShaderManager&&) & noexcept -> ShaderManager& = delete;

  auto index(std::wstring const& name) & noexcept -> result::Result<std::size_t, error::Win32Error>;

  auto shader(std::size_t index) & noexcept -> result::Result<graphics::Shader*, std::nullptr_t>;

  auto shader(std::wstring const& name) & noexcept -> result::Result<graphics::Shader*, error::Win32Error>;

protected:
private:
  graphics::Renderer* m_renderer = nullptr;
  std::unordered_map<std::wstring, std::size_t> m_shader_names = std::unordered_map<std::wstring, std::size_t>();
  std::vector<std::unique_ptr<graphics::Shader>> m_shaders = std::vector<std::unique_ptr<graphics::Shader>>();

  explicit ShaderManager(graphics::Renderer* renderer) noexcept;

  auto load(std::wstring const& name) & noexcept -> result::Result<std::size_t, error::Win32Error>;
};
} // namespace project1::manager
