#pragma once

#include <unordered_map>
#include <vector>

#include "project1/graphics/renderer.hpp"
#include "project1/utility/singleton.hpp"

namespace project1::manager {
class ModelManager final : public utility::Singleton<ModelManager>
{
  friend utility::Singleton<ModelManager>;

public:
  constexpr ModelManager() noexcept = delete;

  constexpr ModelManager(ModelManager const&) noexcept = delete;

  constexpr ModelManager(ModelManager&&) = delete;

  constexpr auto operator=(ModelManager const&) & noexcept -> ModelManager& = delete;

  constexpr auto operator=(ModelManager&&) & noexcept -> ModelManager& = delete;

  auto index(std::wstring const& name) & noexcept -> result::Result<std::size_t, error::Win32Error>;

  auto model(std::size_t index) & noexcept -> result::Result<graphics::Drawable*, std::nullptr_t>;

  auto model(std::wstring const& name) & noexcept -> result::Result<graphics::Drawable*, error::Win32Error>;

protected:
private:
  std::unordered_map<std::wstring, std::size_t> m_model_names = std::unordered_map<std::wstring, std::size_t>();
  std::vector<std::unique_ptr<graphics::Drawable>> m_models = std::vector<std::unique_ptr<graphics::Drawable>>();
  graphics::Renderer* m_renderer = nullptr;

  explicit ModelManager(graphics::Renderer* renderer) noexcept;

  auto load(std::wstring const& name) & noexcept -> result::Result<std::size_t, error::Win32Error>;
};
} // namespace project1::manager
