#pragma once

#include <unordered_map>
#include <vector>

#include "project1/graphics/renderer.hpp"
#include "project1/utility/singleton.hpp"

namespace project1::manager {
class TextureManager : public utility::Singleton<TextureManager>
{
  friend utility::Singleton<TextureManager>;

public:
  constexpr TextureManager() noexcept = delete;

  constexpr TextureManager(TextureManager const&) noexcept = delete;

  constexpr TextureManager(TextureManager&&) = delete;

  constexpr auto operator=(TextureManager const&) & noexcept -> TextureManager& = delete;

  constexpr auto operator=(TextureManager&&) & noexcept -> TextureManager& = delete;

  auto index(std::wstring const& name, UINT slot = 0) & noexcept -> result::Result<std::size_t, error::Win32Error>;

  auto texture(std::size_t index) & noexcept -> result::Result<graphics::Texture*, std::nullptr_t>;

  auto texture(std::wstring const& name, UINT slot = 0) & noexcept -> result::Result<graphics::Texture*, error::Win32Error>;

protected:
private:
  graphics::Renderer* m_renderer = nullptr;
  std::unordered_map<std::wstring, std::size_t> m_texture_names = std::unordered_map<std::wstring, std::size_t>();
  std::vector<std::unique_ptr<graphics::Texture>> m_textures = std::vector<std::unique_ptr<graphics::Texture>>();

  explicit TextureManager(graphics::Renderer* renderer) noexcept;

  auto load(std::wstring const& name, UINT slot) & noexcept -> result::Result<std::size_t, error::Win32Error>;
};
} // namespace project1::manager
