#pragma once

#include <unordered_map>

#include "project1/input/action.hpp"
#include "project1/input/axis.hpp"
#include "project1/input/keyboard.hpp"
#include "project1/input/mouse.hpp"
#include "project1/option/option.hpp"

namespace project1::manager {
class InputManager
{
public:
  // constexpr InputManager() noexcept = delete;

  constexpr InputManager(InputManager const&) noexcept = delete;

  constexpr InputManager(InputManager&&) = delete;

  virtual ~InputManager() noexcept = default;

  constexpr auto operator=(InputManager const&) & noexcept -> InputManager& = delete;

  constexpr auto operator=(InputManager&&) & noexcept -> InputManager& = delete;

  auto action(std::wstring const& name) const& noexcept -> option::Option<input::Action const*>;

  auto action(std::size_t index) const& noexcept -> option::Option<input::Action const*>;

  auto action_index(std::wstring const& name) const& noexcept -> option::Option<std::size_t>;

  auto add_action(input::Action&& action, std::wstring_view name) & noexcept -> void;

  auto add_axis(input::Axis&& axis, std::wstring_view name) & noexcept -> void;

  auto axis(std::wstring const& name) const& noexcept -> option::Option<input::Axis const*>;

  auto axis(std::size_t index) const& noexcept -> option::Option<input::Axis const*>;

  auto axis_index(std::wstring const& name) const& noexcept -> option::Option<std::size_t>;

  auto begin_update() & noexcept -> void;

  auto end_update() & noexcept -> void;

  constexpr auto keyboard() & noexcept -> input::Keyboard& { return m_keyboard; }

  constexpr auto keyboard() const& noexcept -> input::Keyboard const& { return m_keyboard; }

  constexpr auto mouse() & noexcept -> input::Mouse& { return m_mouse; }

  constexpr auto mouse() const& noexcept -> input::Mouse const& { return m_mouse; }

  // static auto new_() noexcept -> InputManager;

  static auto new_pointer() noexcept -> InputManager*;

  auto process_event(input::Event const& event) & noexcept -> void;

protected:
private:
  std::unordered_map<std::wstring, std::size_t> m_action_names = std::unordered_map<std::wstring, std::size_t>();
  std::vector<input::Action> m_actions = std::vector<input::Action>();
  std::vector<input::Axis> m_axes = std::vector<input::Axis>();
  std::unordered_map<std::wstring, std::size_t> m_axis_names = std::unordered_map<std::wstring, std::size_t>();
  std::vector<input::Device*> m_devices;
  input::Keyboard m_keyboard = input::Keyboard::new_();
  input::Mouse m_mouse = input::Mouse::new_();

  explicit InputManager(bool controller) noexcept;
};
}
