#pragma once

namespace project1
{
class Component
{
  public:
    constexpr Component() noexcept = delete;

    Component(const Component &) noexcept = default;

    Component(Component &&) noexcept = default;

    virtual ~Component() noexcept = default;

    Component &operator=(const Component &) noexcept = default;
    Component &operator=(Component &&) noexcept = default;

  protected:
  private:
};
} // namespace project1
