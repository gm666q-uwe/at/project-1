#pragma once

#include <cstddef>
#include <ostream>

namespace project1::error {
struct Errno
{
public:
  constexpr Errno() noexcept = delete;

  constexpr Errno(Errno const&) noexcept = default;

  constexpr Errno(Errno&&) noexcept = default;

  constexpr virtual ~Errno() noexcept = default;

  constexpr auto operator=(Errno const&) & noexcept -> Errno& = default;

  constexpr auto operator=(Errno&&) & noexcept -> Errno& = default;

  friend auto operator<<(std::ostream& os, Errno const& other) -> std::ostream&;

  friend auto operator<<(std::wostream& os, Errno const& other) -> std::wostream&;

  static auto last() noexcept -> Errno;

  static constexpr auto new_(errno_t value) noexcept -> Errno { return Errno(value); }

  [[nodiscard]] constexpr auto value() const& noexcept -> errno_t { return m_value; }

protected:
private:
  errno_t m_value = 0;

  constexpr explicit Errno(errno_t value) noexcept
    : m_value(value)
  {}
};

auto
operator<<(std::ostream& os, Errno const& other) -> std::ostream&;

auto
operator<<(std::wostream& os, Errno const& other) -> std::wostream&;
}
