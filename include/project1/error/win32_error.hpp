#pragma once

#include <ostream>

#include "project1/platform.hpp"

namespace project1::error {
struct Win32Error
{
public:
  constexpr Win32Error() noexcept = delete;

  constexpr Win32Error(Win32Error const&) noexcept = default;

  constexpr Win32Error(Win32Error&&) noexcept = default;

  constexpr virtual ~Win32Error() noexcept = default;

  constexpr auto operator=(Win32Error const&) & noexcept -> Win32Error& = default;

  constexpr auto operator=(Win32Error&&) & noexcept -> Win32Error& = default;

  friend auto operator<<(std::wostream& os, Win32Error const& self) -> std::wostream&;

  static auto last() noexcept -> Win32Error;

  [[nodiscard]] auto message() const& noexcept -> std::wstring;

  static constexpr auto new_(HRESULT value) noexcept -> Win32Error { return Win32Error(value); }

  [[nodiscard]] constexpr auto value() const& noexcept -> HRESULT { return m_value; }

protected:
private:
  HRESULT m_value = 0;

  constexpr explicit Win32Error(HRESULT value) noexcept
    : m_value(value)
  {}
};

auto
operator<<(std::wostream& os, Win32Error const& self) -> std::wostream&;
} // namespace project1::error
