#pragma once

#include <memory>
#include <mutex>
#include <utility>

namespace project1::utility {
template<typename T>
class Singleton
{
public:
  constexpr Singleton(Singleton const&) noexcept = delete;

  constexpr Singleton(Singleton&&) noexcept = delete;

  virtual ~Singleton() noexcept = default;

  constexpr auto operator=(Singleton const&) & noexcept -> Singleton& = delete;

  constexpr auto operator=(Singleton&&) & noexcept -> Singleton& = delete;

  static auto instance() noexcept -> T*
  {
    std::lock_guard<std::mutex> lock_guard(m_instance_mutex);
    if (!m_instance) {
      return nullptr;
    }
    return m_instance.get();
  }

  template<typename... Args>
  static auto instance(Args&&... args) noexcept -> T&
  {
    std::lock_guard<std::mutex> lock_guard(m_instance_mutex);
    if (!m_instance) {
      m_instance = std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }
    return *m_instance;
  }

protected:
  constexpr Singleton() noexcept = default;

private:
  static std::unique_ptr<T> m_instance;
  static std::mutex m_instance_mutex;
};
} // namespace project1::utility