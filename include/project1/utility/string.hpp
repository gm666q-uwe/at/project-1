#pragma once

#include <string>
#include <string_view>
#include <vector>

namespace project1::utility {
template<typename CharT,
	 typename Traits = std::char_traits<CharT>,
	 typename Alloc = std::allocator<std::basic_string<CharT, Traits>>>
auto
string_split(std::basic_string_view<CharT, Traits> string, std::basic_string_view<CharT, Traits> delimiter) noexcept
  -> std::vector<std::basic_string<CharT, Traits>, Alloc>
{
  auto last = static_cast<typename std::basic_string<CharT, Traits>::size_type>(0);
  auto result = std::vector<std::basic_string<CharT, Traits>, Alloc>();
  for (auto next = string.find(delimiter, last); next != std::basic_string_view<CharT, Traits>::npos;
       next = string.find(delimiter, last)) {
    result.emplace_back(string.substr(last, next - last));
    last = next + 1;
  }
  result.emplace_back(string.substr(last));
  return std::move(result);
}
}
