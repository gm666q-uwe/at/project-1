#pragma once

#include <span>
#include <string_view>

#include "project1/platform.hpp"

namespace project1 {
class App
{
public:
  constexpr App(App const&) noexcept = delete;

  App(App&&) noexcept = default;

  virtual ~App() noexcept = default;

  constexpr auto operator=(App const&) & noexcept -> App& = delete;

  auto operator=(App&&) & noexcept -> App& = default;

  virtual auto run(int argc, char const** argv) & noexcept -> int;

  virtual auto run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int;

protected:
  static constexpr double const TARGET_DELTA_TIME = 1.0 / 60.0;

  constexpr App() noexcept = default;

  virtual auto fixed_update(double fixed_delta_time, double fixed_time) & noexcept -> void = 0;

  virtual auto init(std::span<std::wstring_view> args) & noexcept -> void = 0;

  virtual auto input() & noexcept -> void = 0;

  virtual auto late_update(double delta_time, double time) & noexcept -> void = 0;

  virtual auto render(double time) & noexcept -> void = 0;

  [[nodiscard]] virtual auto result() const& noexcept -> int = 0;

  auto run(std::span<std::wstring_view> args) & noexcept -> int;

  [[nodiscard]] virtual auto running() const& noexcept -> bool = 0;

  virtual auto update(double delta_time, double time) & noexcept -> void = 0;

private:
};
} // namespace project1
