#pragma once

#include "project1/graphics/renderer.hpp"
#include "project1/manager/input_manager.hpp"

namespace project1::display {
class Display;

class Window
{
  friend Display;

public:
  constexpr Window() noexcept = delete;

  constexpr Window(Window const&) noexcept = delete;

  Window(Window&& other) noexcept;

  virtual ~Window() noexcept;

  constexpr auto operator=(Window const&) & noexcept -> Window& = delete;

  auto operator=(Window&& other) & noexcept -> Window&;

  [[nodiscard]] auto input_manager() & noexcept -> manager::InputManager *;

  [[nodiscard]] auto input_manager() const& noexcept -> manager::InputManager const*;

  auto renderer() & noexcept -> result::Result<graphics::Renderer*, error::Win32Error>;

  [[nodiscard]] auto run() const& noexcept -> bool;

protected:
  LONG m_height = 0;
  std::unique_ptr<manager::InputManager> m_input_manager;
  std::unique_ptr<graphics::Renderer> m_renderer;
  bool m_running = true;
  LONG m_width = 0;
  HWND m_window = nullptr;

  virtual auto window_procedure(HWND window, UINT message, WPARAM w_parameter, LPARAM l_parameter) & -> LRESULT;

private:
  static constexpr DWORD const M_WINDOW_STYLE = WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU;

  constexpr Window(LONG width, LONG height) noexcept
    : m_width(width)
    , m_height(height)
  {}

  auto drop() & noexcept -> void;

  auto initialize(std::wstring_view title,
		  int show_command,
		  HINSTANCE instance,
		  std::wstring_view window_class_name) & noexcept -> option::Option<error::Win32Error>;

  static auto new_(LONG width,
		   LONG height,
		   std::wstring_view title,
		   int show_command,
		   HINSTANCE instance,
		   std::wstring_view window_class_name) noexcept -> result::Result<Window, error::Win32Error>;

  static auto new_pointer(LONG width,
			  LONG height,
			  std::wstring_view title,
			  int show_command,
			  HINSTANCE instance,
			  std::wstring_view window_class_name) noexcept -> result::Result<Window*, error::Win32Error>;

  static LRESULT CALLBACK window_procedure_proxy(_In_ HWND window,
						 _In_ UINT message,
						 _In_ WPARAM wParam,
						 _In_ LPARAM lParam);
};
} // namespace project1::display
