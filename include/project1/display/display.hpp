#pragma once

#include "project1/display/window.hpp"

namespace project1::display {
class Display
{
public:
  constexpr Display() noexcept = delete;

  constexpr Display(Display const&) noexcept = delete;

  Display(Display&& other) noexcept;

  virtual ~Display() noexcept;

  constexpr auto operator=(Display const&) & noexcept -> Display& = delete;

  auto operator=(Display&& other) & noexcept -> Display&;

  [[nodiscard]] auto create_window(LONG width,
		     LONG height,
		     std::wstring_view title,
		     int show_command = SW_SHOWDEFAULT) const& noexcept -> result::Result<Window, error::Win32Error>;

  [[nodiscard]] auto create_window_pointer(LONG width,
			     LONG height,
			     std::wstring_view title,
			     int show_command = SW_SHOWDEFAULT) const& noexcept
    -> result::Result<Window*, error::Win32Error>;

  static auto new_(HINSTANCE instance,
		   option::Option<std::wstring_view> window_class_name =
		     option::Option<std::wstring_view>::None()) noexcept -> result::Result<Display, error::Win32Error>;

  static auto new_pointer(
    HINSTANCE instance,
    option::Option<std::wstring_view> window_class_name = option::Option<std::wstring_view>::None()) noexcept
    -> result::Result<Display*, error::Win32Error>;

  [[nodiscard]] auto run() & noexcept -> bool;

protected:
private:
  HINSTANCE m_instance = nullptr;
  bool m_running = true;
  ATOM m_window_class_atom = 0;
  std::wstring m_window_class_name = std::wstring();

  Display(HINSTANCE instance, std::wstring_view window_class_name) noexcept;

  auto drop() & noexcept -> void;

  auto initialize() & noexcept -> option::Option<error::Win32Error>;

  static LRESULT CALLBACK window_procedure(_In_ HWND window, _In_ UINT message, _In_ WPARAM wParam, _In_ LPARAM lParam);
};
} // namespace project1::display
