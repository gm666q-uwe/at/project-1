#pragma once

#include <memory>

#include "project1/ecs/ecs.hpp"
#include "project1/app.hpp"
#include "project1/display/display.hpp"

namespace project1 {
class Project1 : public App
{
public:
  constexpr Project1(Project1 const&) noexcept = delete;

  constexpr Project1(Project1&&) noexcept = delete;

  ~Project1() noexcept override;

  constexpr auto operator=(Project1 const&) & noexcept -> Project1& = delete;

  constexpr auto operator=(Project1&&) & noexcept -> Project1& = delete;

  static auto new_() noexcept -> Project1;

  static auto new_pointer() noexcept -> Project1*;

  auto run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int override;

protected:
  auto fixed_update(double fixed_delta_time, double fixed_time) & noexcept -> void override;

  auto init(std::span<std::wstring_view> args) & noexcept -> void override;

  auto input() & noexcept -> void override;

  auto late_update(double delta_time, double time) & noexcept -> void override;

  auto render(double time) & noexcept -> void override;

  [[nodiscard]] auto result() const& noexcept -> int override;

  [[nodiscard]] auto running() const& noexcept -> bool override;

  auto update(double delta_time, double time) & noexcept -> void override;

private:
  std::unique_ptr<display::Display> m_display = nullptr;
  HINSTANCE m_instance = nullptr;
  graphics::Renderer* m_renderer = nullptr;
  int m_result = 0;
  bool m_running = true;
  int m_show_command = SW_SHOWDEFAULT;
  std::unique_ptr<graphics::TransformBuffer<>> m_transform_buffer = nullptr;
  std::unique_ptr<display::Window> m_window = nullptr;

  constexpr Project1() noexcept
    : App()
  {}
};
} // namespace project1
