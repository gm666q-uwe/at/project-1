#pragma once

#include <span>

#include "project1/graphics/buffer.hpp"

namespace project1::graphics {
class Renderer;
namespace mesh {
class Cube;
class TriangleMesh;
} // namespace mesh

class IndexBuffer : public Buffer
{
  friend Renderer;
  friend mesh::Cube;
  friend mesh::TriangleMesh;

public:
  constexpr IndexBuffer() noexcept = delete;

  constexpr IndexBuffer(IndexBuffer const&) noexcept = delete;

  IndexBuffer(IndexBuffer&& other) noexcept;

  ~IndexBuffer() noexcept override;

  constexpr auto operator=(IndexBuffer const&) & noexcept -> IndexBuffer& = delete;

  auto operator=(IndexBuffer&& other) & noexcept -> IndexBuffer&;

  auto bind() const& noexcept -> void override;

  auto is_bound() const& noexcept -> bool override;

  auto size() const& noexcept -> std::size_t;

  auto unbind() const& noexcept -> void override;

protected:
private:
  std::size_t m_size = 0;

  constexpr IndexBuffer(ID3D11Device* device, ID3D11DeviceContext* device_context, std::size_t size) noexcept
    : Buffer(device, device_context)
    , m_size(size)
  {}

  auto initialize(std::span<const std::uint16_t> indices) & noexcept -> option::Option<error::Win32Error>;

  static auto new_(ID3D11Device* device,
		   ID3D11DeviceContext* device_context,
		   std::span<const std::uint16_t> indices) noexcept -> result::Result<IndexBuffer, error::Win32Error>;

  static auto new_pointer(ID3D11Device* device,
			  ID3D11DeviceContext* device_context,
			  std::span<const std::uint16_t> indices) noexcept
    -> result::Result<IndexBuffer*, error::Win32Error>;
};
} // namespace project1::graphics
