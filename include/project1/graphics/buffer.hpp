#pragma once

#include "project1/error/win32_error.hpp"
#include "project1/graphics/bindable.hpp"
#include "project1/result/result.hpp"

namespace project1::graphics {
class Buffer : public Bindable
{
public:
  constexpr Buffer() noexcept = delete;

  constexpr Buffer(Buffer const&) noexcept = delete;

  Buffer(Buffer&& other) noexcept;

  virtual ~Buffer() noexcept;

  constexpr auto operator=(Buffer const&) & noexcept -> Buffer& = delete;

  auto operator=(Buffer&& other) & noexcept -> Buffer&;

protected:
  ID3D11Buffer* m_buffer = nullptr;
  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;

  constexpr Buffer(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
    : m_device(device)
    , m_device_context(device_context)
  {}

  auto drop() & noexcept -> void;

  auto initialize_base(UINT bind_flags,
		       UINT cpu_access_flags,
		       void const* data,
		       UINT size,
		       UINT stride,
		       D3D11_USAGE usage) & noexcept -> option::Option<error::Win32Error>;

private:
};
} // namespace project1::graphics
