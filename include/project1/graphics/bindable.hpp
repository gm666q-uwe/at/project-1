#pragma once

namespace project1::graphics {
class Bindable
{
public:
  virtual auto bind() const& noexcept -> void = 0;
  [[nodiscard]] virtual auto is_bound() const& noexcept -> bool = 0;
  virtual auto unbind() const& noexcept -> void = 0;

protected:
private:
};
} // namespace project1::graphics
