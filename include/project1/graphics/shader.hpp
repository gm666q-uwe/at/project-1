#pragma once

#include <string_view>

#include "project1/error/win32_error.hpp"
#include "project1/graphics/bindable.hpp"
#include "project1/result/result.hpp"

namespace project1::graphics {
class Renderer;

class Shader : public Bindable
{
  friend Renderer;

public:
  enum class Type : std::uint8_t
  {
    Compute = 0,
    Domain = 1,
    Geometry = 2,
    Hull = 3,
    Pixel = 4,
    Vertex = 5,
  };

  constexpr Shader() noexcept = delete;

  constexpr Shader(Shader const&) noexcept = delete;

  Shader(Shader&& other) noexcept;

  virtual ~Shader() noexcept;

  constexpr auto operator=(Shader const&) & noexcept -> Shader& = delete;

  auto operator=(Shader&& other) & noexcept -> Shader&;

  auto bind() const& noexcept -> void override;

  auto is_bound() const& noexcept -> bool override;

  auto unbind() const& noexcept -> void override;

protected:
private:
  static constexpr std::array<char const*, 6> M_TYPE_TARGETS = { "cs_5_0", "ds_5_0", "gs_5_0",
								 "hs_5_0", "ps_5_0", "vs_5_0" };

  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;
  ID3D11InputLayout* m_input_layout = nullptr;
  ID3D11PixelShader* m_pixel_shader = nullptr;
  ID3D11VertexShader* m_vertex_shader = nullptr;

  constexpr Shader(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
    : m_device(device)
    , m_device_context(device_context)
  {}

  auto compile(std::wstring_view shader_file_path, Type type) const& noexcept
    -> result::Result<ID3DBlob*, error::Win32Error>;

  auto drop() & noexcept -> void;

  auto initialize(std::wstring_view vertex_shader_file_path, std::wstring_view pixel_shader_file_path) & noexcept
    -> option::Option<error::Win32Error>;

  static auto new_(ID3D11Device* device,
		   ID3D11DeviceContext* device_context,
		   std::wstring_view vertex_shader_file_path,
		   std::wstring_view pixel_shader_file_path) noexcept -> result::Result<Shader, error::Win32Error>;

  static auto new_pointer(ID3D11Device* device,
			  ID3D11DeviceContext* device_context,
			  std::wstring_view vertex_shader_file_path,
			  std::wstring_view pixel_shader_file_path) noexcept
    -> result::Result<Shader*, error::Win32Error>;
};
} // namespace project1::graphics
