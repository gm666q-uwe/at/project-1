#pragma once

#include <DirectXMath/DirectXMath.h>

namespace project1::graphics {
struct Vertex
{
public:
  DirectX::XMFLOAT3 position;
  DirectX::XMFLOAT2 uv;
  DirectX::XMFLOAT3 normal;

  constexpr bool operator==(const Vertex& rhs) const noexcept
  {
    return position.x == rhs.position.x && position.y == rhs.position.y && position.z == rhs.position.z &&
	   uv.x == rhs.uv.x && uv.y == rhs.uv.y && normal.x == rhs.normal.x && normal.y == rhs.normal.y &&
	   normal.z == rhs.normal.z;
  }

  constexpr bool operator!=(const Vertex& rhs) const noexcept { return !(rhs == *this); }

protected:
private:
};
} // namespace project1::graphics
