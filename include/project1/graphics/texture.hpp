#pragma once

#include <string_view>

#include "project1/error/win32_error.hpp"
#include "project1/graphics/bindable.hpp"
#include "project1/result/result.hpp"

namespace project1::graphics {
class Renderer;

class Texture : public Bindable
{
  friend Renderer;

public:
  constexpr Texture() noexcept = delete;

  constexpr Texture(Texture const&) noexcept = delete;

  Texture(Texture&& other) noexcept;

  virtual ~Texture() noexcept;

  constexpr auto operator=(Texture const&) & noexcept -> Texture& = delete;

  auto operator=(Texture&& other) & noexcept -> Texture&;

  auto bind() const& noexcept -> void override;

  [[nodiscard]] auto is_bound() const& noexcept -> bool override;

  auto unbind() const& noexcept -> void override;

protected:
private:
  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;
  ID3D11SamplerState* m_sampler_state = nullptr;
  UINT m_slot = 0;
  ID3D11ShaderResourceView* m_texture_view = nullptr;

  constexpr Texture(ID3D11Device* device, ID3D11DeviceContext* device_context, UINT slot) noexcept
    : m_device(device)
    , m_device_context(device_context)
    , m_slot(slot)
  {}

  auto drop() & noexcept -> void;

  auto initialize(std::wstring_view texture_file_path) & noexcept -> option::Option<error::Win32Error>;

  static auto new_(ID3D11Device* device,
		   ID3D11DeviceContext* device_context,
		   UINT slot,
		   std::wstring_view texture_file_path) noexcept -> result::Result<Texture, error::Win32Error>;

  static auto new_pointer(ID3D11Device* device,
			  ID3D11DeviceContext* device_context,
			  UINT slot,
			  std::wstring_view texture_file_path) noexcept -> result::Result<Texture*, error::Win32Error>;
};
}