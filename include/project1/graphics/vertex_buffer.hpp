#pragma once

#include <span>

#include "project1/graphics/buffer.hpp"
#include "project1/graphics/vertex.hpp"

namespace project1::graphics {
class Renderer;
namespace mesh {
class Cube;
class TriangleMesh;
} // namespace mesh

class VertexBuffer final: public Buffer
{
  friend Renderer;
  friend mesh::Cube;
  friend mesh::TriangleMesh;

public:
  constexpr VertexBuffer() noexcept = delete;

  constexpr VertexBuffer(VertexBuffer const&) noexcept = delete;

  VertexBuffer(VertexBuffer&& other) noexcept;

  ~VertexBuffer() noexcept final;

  constexpr auto operator=(VertexBuffer const&) & noexcept -> VertexBuffer& = delete;

  auto operator=(VertexBuffer&& other) & noexcept -> VertexBuffer&;

  auto bind() const& noexcept -> void final;

  [[nodiscard]] auto is_bound() const& noexcept -> bool final;

  auto unbind() const& noexcept -> void final;

protected:
private:
  constexpr VertexBuffer(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
    : Buffer(device, device_context)
  {}

  auto initialize(std::span<const Vertex> vertices) & noexcept -> option::Option<error::Win32Error>;

  static auto new_(ID3D11Device* device, ID3D11DeviceContext* device_context, std::span<const Vertex> vertices) noexcept
    -> result::Result<VertexBuffer, error::Win32Error>;

  static auto new_pointer(ID3D11Device* device,
			  ID3D11DeviceContext* device_context,
			  std::span<const Vertex> vertices) noexcept
    -> result::Result<VertexBuffer*, error::Win32Error>;
};
} // namespace project1::graphics
