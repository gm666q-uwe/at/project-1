#pragma once

#include "project1/graphics/bindable.hpp"

namespace project1::graphics {
class Drawable : public Bindable
{
public:
  virtual auto draw() const& noexcept -> void = 0;

protected:
private:
};
} // namespace project1::graphics
