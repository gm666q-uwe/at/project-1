#pragma once

#include "project1/graphics/buffer.hpp"
#include "project1/graphics/shader.hpp"
#include "project1/graphics/transform.hpp"

namespace project1::graphics {
class Renderer;

template<typename T, Shader::Type S>
class ConstantBuffer : public Buffer
{
  friend Renderer;

public:
  constexpr ConstantBuffer() noexcept = delete;

  constexpr ConstantBuffer(ConstantBuffer<T, S> const&) noexcept = delete;

  ConstantBuffer(ConstantBuffer<T, S>&& other) noexcept
    : Buffer(std::move(other))
    , m_slot(std::move(other.m_slot))
  {}

  ~ConstantBuffer() noexcept override
  {
    if (is_bound()) {
      unbind();
    }

    drop();
  }

  constexpr auto operator=(ConstantBuffer<T, S> const&) & noexcept -> ConstantBuffer<T, S>& = delete;

  auto operator=(ConstantBuffer<T, S>&& other) & noexcept -> ConstantBuffer<T, S>&
  {
    if (this == &Buffer::operator=(std::move(other))) {
      return *this;
    }

    m_slot = std::move(other.m_slot);

    return *this;
  }

  auto bind() const& noexcept -> void override
  {
    switch (S) {
      case Shader::Type::Compute:
	m_device_context->CSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      case Shader::Type::Domain:
	m_device_context->DSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      case Shader::Type::Geometry:
	m_device_context->GSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      case Shader::Type::Hull:
	m_device_context->HSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      case Shader::Type::Pixel:
	m_device_context->PSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      case Shader::Type::Vertex:
	m_device_context->VSSetConstantBuffers(m_slot, 1, &m_buffer);
	break;
      default:
	return;
    }
  }

  auto is_bound() const& noexcept -> bool override
  {
    if (m_buffer == nullptr) {
      return false;
    }
    ID3D11Buffer* buffer = nullptr;
    switch (S) {
      case Shader::Type::Compute:
	m_device_context->CSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      case Shader::Type::Domain:
	m_device_context->DSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      case Shader::Type::Geometry:
	m_device_context->GSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      case Shader::Type::Hull:
	m_device_context->HSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      case Shader::Type::Pixel:
	m_device_context->PSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      case Shader::Type::Vertex:
	m_device_context->VSGetConstantBuffers(m_slot, 1, &buffer);
	break;
      default:
	return false;
    }
    return m_buffer == buffer;
  }

  auto unbind() const& noexcept -> void override
  {
    switch (S) {
      case Shader::Type::Compute:
	m_device_context->CSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      case Shader::Type::Domain:
	m_device_context->DSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      case Shader::Type::Geometry:
	m_device_context->GSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      case Shader::Type::Hull:
	m_device_context->HSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      case Shader::Type::Pixel:
	m_device_context->PSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      case Shader::Type::Vertex:
	m_device_context->VSSetConstantBuffers(m_slot, 0, nullptr);
	break;
      default:
	return;
    }
  }

protected:
private:
  UINT m_slot = 0;

  constexpr ConstantBuffer(ID3D11Device* device, ID3D11DeviceContext* device_context, UINT slot) noexcept
    : Buffer(device, device_context)
    , m_slot(slot)
  {}

  auto initialize() & noexcept -> option::Option<error::Win32Error>
  {
    return initialize_base(
      D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, nullptr, sizeof(T), 0, D3D11_USAGE_DYNAMIC);
  }

  auto initialize(T const& data) & noexcept -> option::Option<error::Win32Error>
  {
    return initialize_base(
      D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE, &data, sizeof(T), 0, D3D11_USAGE_DYNAMIC);
  }

  static auto new_(ID3D11Device* device, ID3D11DeviceContext* device_context, UINT slot) noexcept
    -> result::Result<ConstantBuffer<T, S>, error::Win32Error>
  {
    auto self = ConstantBuffer(device, device_context, slot);
    if (auto result = self.initialize(); result.is_some()) {
      return std::move(result::Result<ConstantBuffer<T, S>, error::Win32Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<ConstantBuffer<T, S>, error::Win32Error>::Ok(std::move(self)));
  }

  static auto new_pointer(ID3D11Device* device, ID3D11DeviceContext* device_context, UINT slot) noexcept
    -> result::Result<ConstantBuffer<T, S>*, error::Win32Error>
  {
    auto self = new ConstantBuffer<T, S>(device, device_context, slot);
    if (auto result = self->initialize(); result.is_some()) {
      delete self;
      return std::move(result::Result<ConstantBuffer<T, S>*, error::Win32Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<ConstantBuffer<T, S>*, error::Win32Error>::Ok(std::move(self)));
  }

  static auto with_data(ID3D11Device* device, ID3D11DeviceContext* device_context, T const& data, UINT slot) noexcept
    -> result::Result<ConstantBuffer<T, S>, error::Win32Error>
  {
    auto self = ConstantBuffer(device, device_context, slot);
    if (auto result = self.initialize(data); result.is_some()) {
      return std::move(result::Result<ConstantBuffer<T, S>, error::Win32Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<ConstantBuffer<T, S>, error::Win32Error>::Ok(std::move(self)));
  }

  static auto with_data_pointer(ID3D11Device* device,
				ID3D11DeviceContext* device_context,
				T const& data,
				UINT slot) noexcept -> result::Result<ConstantBuffer<T, S>*, error::Win32Error>
  {
    auto self = new ConstantBuffer<T, S>(device, device_context, slot);
    if (auto result = self->initialize(data); result.is_some()) {
      delete self;
      return std::move(result::Result<ConstantBuffer<T, S>*, error::Win32Error>::Err(std::move(result).unwrap()));
    }
    return std::move(result::Result<ConstantBuffer<T, S>*, error::Win32Error>::Ok(std::move(self)));
  }
};

template<Shader::Type S = Shader::Type::Vertex>
using TransformBuffer = ConstantBuffer<Transform, S>;
} // namespace project1::graphics
