#pragma once

#include <DirectXMath/DirectXMath.h>

namespace project1::graphics {
struct Transform
{
public:
  DirectX::XMFLOAT4X4A view_projection;
  DirectX::XMFLOAT4X4A geometry;

protected:
private:
};
} // namespace project1::graphics
