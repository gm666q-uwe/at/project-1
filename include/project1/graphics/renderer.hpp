#pragma once

#include "project1/graphics/constant_buffer.hpp"
#include "project1/graphics/mesh/cube.hpp"
#include "project1/graphics/mesh/triangle_mesh.hpp"
#include "project1/graphics/shader.hpp"
#include "project1/graphics/texture.hpp"

namespace project1 {
namespace display {
class Window;
}

namespace graphics {
class Renderer
{
  friend display::Window;

public:
  constexpr Renderer(Renderer const&) noexcept = delete;

  Renderer(Renderer&& other) noexcept;

  virtual ~Renderer() noexcept;

  constexpr auto operator=(Renderer const&) & noexcept -> Renderer& = delete;

  auto operator=(Renderer&& other) & noexcept -> Renderer&;

  auto begin_frame() const& noexcept -> void;

  auto clear(DirectX::XMVECTORF32 const& color) const& noexcept -> void;

  auto clear_color(DirectX::XMVECTORF32 const& color) const& noexcept -> void;

  auto clear_depth() const& noexcept -> void;

  template<typename T, Shader::Type S>
  auto create_constant_buffer(UINT slot) const& noexcept -> result::Result<ConstantBuffer<T, S>, error::Win32Error>
  {
    return std::move(ConstantBuffer<T, S>::new_(m_device, m_device_context, slot));
  }

  template<typename T, Shader::Type S>
  auto create_constant_buffer_with_data(T const& data, UINT slot) const& noexcept
    -> result::Result<ConstantBuffer<T, S>, error::Win32Error>
  {
    return std::move(ConstantBuffer<T, S>::with_data(m_device, m_device_context, data, slot));
  }

  template<typename T, Shader::Type S>
  auto create_constant_buffer_pointer(UINT slot) const& noexcept
    -> result::Result<ConstantBuffer<T, S>*, error::Win32Error>
  {
    return std::move(ConstantBuffer<T, S>::new_pointer(m_device, m_device_context, slot));
  }

  template<typename T, Shader::Type S>
  auto create_constant_buffer_with_data_pointer(T const& data, UINT slot) const& noexcept
    -> result::Result<ConstantBuffer<T, S>*, error::Win32Error>
  {
    return std::move(ConstantBuffer<T, S>::with_data_pointer(m_device, m_device_context, data, slot));
  }

  [[nodiscard]] auto create_cube() const& noexcept -> result::Result<mesh::Cube, error::Win32Error>;

  [[nodiscard]] auto create_cube_pointer() const& noexcept -> result::Result<mesh::Cube*, error::Win32Error>;

  [[nodiscard]] auto create_index_buffer(std::span<const std::uint16_t> indices) const& noexcept
    -> result::Result<IndexBuffer, error::Win32Error>;

  [[nodiscard]] auto create_index_buffer_pointer(std::span<const std::uint16_t> indices) const& noexcept
    -> result::Result<IndexBuffer*, error::Win32Error>;

  [[nodiscard]] auto create_shader(std::wstring_view vertex_shader_file_path,
		     std::wstring_view pixel_shader_file_path) const& noexcept
    -> result::Result<Shader, error::Win32Error>;

  [[nodiscard]] auto create_shader_pointer(std::wstring_view vertex_shader_file_path,
			     std::wstring_view pixel_shader_file_path) const& noexcept
    -> result::Result<Shader*, error::Win32Error>;

  [[nodiscard]] auto create_texture(UINT slot, std::wstring_view texture_file_path) const& noexcept
    -> result::Result<Texture, error::Win32Error>;

  [[nodiscard]] auto create_texture_pointer(UINT slot, std::wstring_view texture_file_path) const& noexcept
    -> result::Result<Texture*, error::Win32Error>;

  [[nodiscard]] auto create_vertex_buffer(std::span<const Vertex> vertices) const& noexcept
    -> result::Result<VertexBuffer, error::Win32Error>;

  [[nodiscard]] auto create_vertex_buffer_pointer(std::span<const Vertex> vertices) const& noexcept
    -> result::Result<VertexBuffer*, error::Win32Error>;

  auto end_frame() const& noexcept -> void;

  [[nodiscard]] auto load_triangle_mesh_obj(std::wstring_view mesh_file_path) const& noexcept
    -> result::Result<mesh::TriangleMesh, error::Win32Error>;

  [[nodiscard]] auto load_triangle_mesh_obj_pointer(std::wstring_view mesh_file_path) const& noexcept
    -> result::Result<mesh::TriangleMesh*, error::Win32Error>;

  [[nodiscard]] auto perspective_matrix() const& noexcept -> DirectX::XMMATRIX const&;

protected:
private:
  ID3D11DepthStencilView* m_depth_stencil_view = nullptr;
  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;
  DirectX::XMMATRIX m_perspective_matrix = DirectX::XMMATRIX{};
  ID3D11RenderTargetView* m_render_target_view = nullptr;
  IDXGISwapChain* m_swap_chain = nullptr;

  constexpr Renderer() noexcept = default;

  auto drop() & noexcept -> void;

  auto initialize(UINT width, UINT height, HWND window) & noexcept -> option::Option<error::Win32Error>;

  static auto new_(UINT width, UINT height, HWND window) noexcept -> result::Result<Renderer, error::Win32Error>;

  static auto new_pointer(UINT width, UINT height, HWND window) noexcept
    -> result::Result<Renderer*, error::Win32Error>;
};
} // namespace graphics
} // namespace project1