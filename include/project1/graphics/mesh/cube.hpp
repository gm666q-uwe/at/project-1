#pragma once

#include <array>

#include "project1/graphics/drawable.hpp"
#include "project1/graphics/index_buffer.hpp"
#include "project1/graphics/vertex_buffer.hpp"

namespace project1::graphics {
class Renderer;

namespace mesh {
class Cube : public Drawable
{
  friend Renderer;

public:
  constexpr Cube() noexcept = delete;

  constexpr Cube(Cube const&) noexcept = delete;

  Cube(Cube&& other) noexcept;

  virtual ~Cube() noexcept;

  constexpr auto operator=(Cube const&) & noexcept -> Cube& = delete;

  auto operator=(Cube&& other) & noexcept -> Cube&;

  auto bind() const& noexcept -> void override;

  auto draw() const& noexcept -> void override;

  auto is_bound() const& noexcept -> bool override;

  auto unbind() const& noexcept -> void override;

protected:
private:
  static constexpr std::array<std::uint16_t, 36> const M_INDICES = { 0,	 1,  2,	 3,  4,	 5,  6,	 7,  8,	 9,  10, 11,
								     12, 13, 14, 15, 16, 17, 0,	 18, 1,	 3,  19, 4,
								     6,	 20, 7,	 9,  21, 10, 12, 22, 13, 15, 23, 16 };
  static constexpr std::array<Vertex, 24> const M_VERTICES = {
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.875, 0.5 }, DirectX::XMFLOAT3{ 0, 1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.625, 0.75 }, DirectX::XMFLOAT3{ 0, 1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.625, 0.5 }, DirectX::XMFLOAT3{ 0, 1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.625, 0.75 }, DirectX::XMFLOAT3{ 0, 0, 1 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.375, 1 }, DirectX::XMFLOAT3{ 0, 0, 1 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.375, 0.75 }, DirectX::XMFLOAT3{ 0, 0, 1 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.625, 0 }, DirectX::XMFLOAT3{ -1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.375, 0.25 }, DirectX::XMFLOAT3{ -1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.375, 0 }, DirectX::XMFLOAT3{ -1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.375, 0.5 }, DirectX::XMFLOAT3{ 0, -1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.125, 0.75 }, DirectX::XMFLOAT3{ 0, -1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.125, 0.5 }, DirectX::XMFLOAT3{ 0, -1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.625, 0.5 }, DirectX::XMFLOAT3{ 1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.375, 0.75 }, DirectX::XMFLOAT3{ 1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.375, 0.5 }, DirectX::XMFLOAT3{ 1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.625, 0.25 }, DirectX::XMFLOAT3{ 0, 0, -1 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.375, 0.5 }, DirectX::XMFLOAT3{ 0, 0, -1 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, -0.5, -0.5 }, DirectX::XMFLOAT2{ 0.375, 0.25 }, DirectX::XMFLOAT3{ 0, 0, -1 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.875, 0.75 }, DirectX::XMFLOAT3{ 0, 1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.625, 1 }, DirectX::XMFLOAT3{ 0, 0, 1 } },
    Vertex{ DirectX::XMFLOAT3{ -0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.625, 0.25 }, DirectX::XMFLOAT3{ -1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, -0.5, 0.5 }, DirectX::XMFLOAT2{ 0.375, 0.75 }, DirectX::XMFLOAT3{ 0, -1, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, 0.5 }, DirectX::XMFLOAT2{ 0.625, 0.75 }, DirectX::XMFLOAT3{ 1, 0, 0 } },
    Vertex{ DirectX::XMFLOAT3{ 0.5, 0.5, -0.5 }, DirectX::XMFLOAT2{ 0.625, 0.5 }, DirectX::XMFLOAT3{ 0, 0, -1 } }
  };

  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;
  IndexBuffer m_index_buffer;
  VertexBuffer m_vertex_buffer;

  Cube(ID3D11Device* device,
       ID3D11DeviceContext* device_context,
       IndexBuffer&& index_buffer,
       VertexBuffer&& vertex_buffer) noexcept;

  static auto new_(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
    -> result::Result<Cube, error::Win32Error>;

  static auto new_pointer(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
    -> result::Result<Cube*, error::Win32Error>;
};
} // namespace mesh
} // namespace project1::graphics
