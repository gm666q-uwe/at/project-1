#pragma once

#include <string_view>

#include "project1/graphics/drawable.hpp"
#include "project1/graphics/index_buffer.hpp"
#include "project1/graphics/vertex_buffer.hpp"

namespace project1::graphics {
class Renderer;

namespace mesh {
class TriangleMesh : public Drawable
{
  friend Renderer;

public:
  constexpr TriangleMesh() noexcept = delete;

  constexpr TriangleMesh(TriangleMesh const&) noexcept = delete;

  TriangleMesh(TriangleMesh&& other) noexcept;

  virtual ~TriangleMesh() noexcept;

  constexpr auto operator=(TriangleMesh const&) & noexcept -> TriangleMesh& = delete;

  auto operator=(TriangleMesh&& other) & noexcept -> TriangleMesh&;

  auto bind() const& noexcept -> void override;

  auto draw() const& noexcept -> void override;

  auto is_bound() const& noexcept -> bool override;

  auto unbind() const& noexcept -> void override;

protected:
private:
  ID3D11Device* m_device = nullptr;
  ID3D11DeviceContext* m_device_context = nullptr;
  IndexBuffer m_index_buffer;
  VertexBuffer m_vertex_buffer;

  TriangleMesh(ID3D11Device* device,
	       ID3D11DeviceContext* device_context,
	       IndexBuffer&& index_buffer,
	       VertexBuffer&& vertex_buffer) noexcept;

  static auto initialize_obj(std::wstring_view mesh_file_path,
			     std::vector<std::uint16_t>& indices,
			     std::vector<Vertex>& vertices) noexcept -> option::Option<error::Win32Error>;

  static auto load_obj(ID3D11Device* device,
		       ID3D11DeviceContext* device_context,
		       std::wstring_view mesh_file_path) noexcept -> result::Result<TriangleMesh, error::Win32Error>;

  static auto load_obj_pointer(ID3D11Device* device,
			       ID3D11DeviceContext* device_context,
			       std::wstring_view mesh_file_path) noexcept
    -> result::Result<TriangleMesh*, error::Win32Error>;
};
} // namespace mesh
} // namespace project1::graphics
