cbuffer cbPerFrame : register(b0)
{
    float4x4 mat_v_p;
    float4x4 mat_geo;
};

struct VSInput
{
    float3 position : Position;
    float2 uv : UV;
    float3 normal: Normal;
};

struct VSOutput
{
    float2 uv : UV;
    float3 normal : Normal;
    float4 position : SV_Position;
};

VSOutput main(VSInput vin)
{
    VSOutput vout = (VSOutput)0;

    vout.uv = vin.uv;
    vout.normal = vin.normal;
    vout.position = mul(mul(float4(vin.position, 1.0f), mat_geo), mat_v_p);

    return vout;
}
