Texture2D base : register(t0);
SamplerState base_sampler;

struct PSInput {
    float2 uv : UV;
    float3 normal : Normal;
};

float4 main(PSInput pin) : SV_Target
{
    return base.Sample(base_sampler, pin.uv);
    //return 0.5f * (float4(normalize(pin.normal), 1.0f) + float4(1.0f, 1.0f, 1.0f, 1.0f));
}
