#pragma once

// Std
#include <algorithm>
#include <array>
#include <chrono>
#include <codecvt>
#include <iostream>
#include <locale>
#include <memory>
#include <optional>
#include <ostream>
#include <span>
#include <string>
#include <variant>
#include <vector>

// Win32
#include <sdkddkver.h>

#define WIN32_LEAN_AND_MEAN
#include <d3d11.h>
#include <d3dcompiler.h>
#include <dxgi.h>
#include <shellapi.h>
#include <windows.h>
//#include <wrl.h>

// Vcpkg

// DirectXMath
#include <DirectXMath/DirectXCollision.h>
#include <DirectXMath/DirectXColors.h>
#include <DirectXMath/DirectXMath.h>
#include <DirectXMath/DirectXPackedVector.h>

// tinyobjloader
#include <tiny_obj_loader.h>
