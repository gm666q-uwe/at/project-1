#include "project1/display/window.hpp"

project1::display::Window::Window(project1::display::Window&& other) noexcept
  : m_height(other.m_height)
  , m_input_manager(std::move(other.m_input_manager))
  , m_renderer(std::move(other.m_renderer))
  , m_running(std::exchange(other.m_running, false))
  , m_width(other.m_width)
  , m_window(std::exchange(other.m_window, nullptr))
{
  if (m_window != nullptr) {
    SetWindowLongPtrW(m_window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
  }
}

project1::display::Window::~Window() noexcept
{
  drop();
}

auto
project1::display::Window::operator=(project1::display::Window&& other) & noexcept -> project1::display::Window&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_height = other.m_height;
  m_input_manager = std::move(m_input_manager);
  m_renderer = std::move(m_renderer);
  m_running = std::exchange(other.m_running, m_running);
  m_width = other.m_width;
  m_window = std::exchange(other.m_window, m_window);

  if (m_window != nullptr) {
    SetWindowLongPtrW(m_window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
  }

  return *this;
}

auto
project1::display::Window::drop() & noexcept -> void
{
  m_running = false;
  m_input_manager.reset();
  m_renderer.reset();
  if (m_window != nullptr) {
    DestroyWindow(m_window);
    m_window = nullptr;
  }
}

auto
project1::display::Window::initialize(std::wstring_view title,
				      int show_command,
				      HINSTANCE instance,
				      std::wstring_view window_class_name) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  m_input_manager = std::unique_ptr<manager::InputManager>(manager::InputManager::new_pointer());

  auto window_rect = RECT{ 0, 0, m_width, m_height };
  AdjustWindowRectEx(&window_rect, M_WINDOW_STYLE, FALSE, 0);
  m_window = CreateWindowExW(0,
			     window_class_name.data(),
			     title.data(),
			     M_WINDOW_STYLE,
			     CW_USEDEFAULT,
			     CW_USEDEFAULT,
			     window_rect.right - window_rect.left,
			     window_rect.bottom - window_rect.top,
			     nullptr,
			     nullptr,
			     instance,
			     this);

  if (m_window == nullptr) {
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::last()));
  }

  ShowWindow(m_window, show_command);

  return std::move(option::Option<error::Win32Error>::None());
}

auto
project1::display::Window::input_manager() & noexcept -> project1::manager::InputManager*
{
  return m_input_manager.get();
}

auto
project1::display::Window::input_manager() const& noexcept -> project1::manager::InputManager const*
{
  return m_input_manager.get();
}

auto
project1::display::Window::new_(LONG width,
				LONG height,
				std::wstring_view title,
				int show_command,
				HINSTANCE instance,
				std::wstring_view window_class_name) noexcept
  -> project1::result::Result<project1::display::Window, project1::error::Win32Error>
{
  auto self = Window(width, height);
  if (auto result = self.initialize(title, show_command, instance, window_class_name); result.is_some()) {
    return std::move(result::Result<Window, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::display::Window::new_pointer(LONG width,
				       LONG height,
				       std::wstring_view title,
				       int show_command,
				       HINSTANCE instance,
				       std::wstring_view window_class_name) noexcept
  -> project1::result::Result<project1::display::Window*, project1::error::Win32Error>
{
  auto* self = new Window(width, height);
  if (auto result = self->initialize(title, show_command, instance, window_class_name); result.is_some()) {
    delete self;
    return std::move(result::Result<Window*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Window*, error::Win32Error>::Ok(self));
}

auto
project1::display::Window::renderer() & noexcept
  -> project1::result::Result<project1::graphics::Renderer*, project1::error::Win32Error>
{
  if (!m_renderer) {
    if (auto result = graphics::Renderer::new_pointer(m_width, m_height, m_window); result.is_err()) {
      return std::move(result::Result<graphics::Renderer*, error::Win32Error>::Err(std::move(result).unwrap_err()));
    } else {
      m_renderer = std::unique_ptr<graphics::Renderer>(std::move(result).unwrap());
    }
  }
  return std::move(result::Result<graphics::Renderer*, error::Win32Error>::Ok(m_renderer.get()));
}

auto
project1::display::Window::run() const& noexcept -> bool
{
  return m_running;
}

auto
project1::display::Window::window_procedure(HWND window,
					    UINT message,
					    WPARAM w_parameter,
					    LPARAM l_parameter) & -> LRESULT
{
  switch (message) {
    case WM_CHAR:
      m_input_manager->keyboard().process_char(w_parameter, l_parameter);
      return 0;
    case WM_CLOSE:
      m_running = false;
      PostQuitMessage(0);
      return 0;
    case WM_KEYDOWN:
    case WM_KEYUP:
      m_input_manager->keyboard().process_key(w_parameter, l_parameter);
      return 0;
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
      m_input_manager->mouse().process_left_button(w_parameter, l_parameter);
      return 0;
    case WM_MBUTTONDOWN:
    case WM_MBUTTONUP:
      m_input_manager->mouse().process_middle_button(w_parameter, l_parameter);
      return 0;
    case WM_MOUSEMOVE:
      m_input_manager->mouse().process_move(w_parameter, l_parameter);
      return 0;
    case WM_RBUTTONDOWN:
    case WM_RBUTTONUP:
      m_input_manager->mouse().process_right_button(w_parameter, l_parameter);
      return 0;
    case WM_XBUTTONDOWN:
    case WM_XBUTTONUP:
      m_input_manager->mouse().process_x_button(w_parameter, l_parameter);
      return 0;
    /*case WM_SETCURSOR:
      return FALSE;*/
    default:
      std::wcout << L"Unknown message: " << message << std::endl;
      break;
  }
  return DefWindowProcW(window, message, w_parameter, l_parameter);
}

LRESULT
project1::display::Window::window_procedure_proxy(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
  return reinterpret_cast<Window*>(GetWindowLongPtrW(window, GWLP_USERDATA))
    ->window_procedure(window, message, wParam, lParam);
}
