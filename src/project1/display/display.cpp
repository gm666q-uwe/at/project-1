#include "project1/display/display.hpp"

project1::display::Display::Display(HINSTANCE instance, std::wstring_view window_class_name) noexcept
  : m_instance(instance)
  , m_window_class_name(window_class_name)
{}

project1::display::Display::Display(project1::display::Display&& other) noexcept
  : m_instance(other.m_instance)
  , m_running(std::exchange(other.m_running, false))
  , m_window_class_atom(std::exchange(other.m_window_class_atom, 0))
  , m_window_class_name(std::move(other.m_window_class_name))
{}

project1::display::Display::~Display() noexcept
{
  drop();
}

auto
project1::display::Display::operator=(project1::display::Display&& other) & noexcept -> project1::display::Display&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_instance = other.m_instance;
  m_running = std::exchange(other.m_running, m_running);
  m_window_class_atom = std::exchange(other.m_window_class_atom, m_window_class_atom);
  m_window_class_name = std::move(other.m_window_class_name);

  return *this;
}

auto
project1::display::Display::create_window(LONG width,
					  LONG height,
					  std::wstring_view title,
					  int show_command) const& noexcept
  -> project1::result::Result<project1::display::Window, project1::error::Win32Error>
{
  return std::move(Window::new_(width, height, title, show_command, m_instance, m_window_class_name));
}

auto
project1::display::Display::create_window_pointer(LONG width,
						  LONG height,
						  std::wstring_view title,
						  int show_command) const& noexcept
  -> project1::result::Result<project1::display::Window*, project1::error::Win32Error>
{
  return std::move(Window::new_pointer(width, height, title, show_command, m_instance, m_window_class_name));
}

auto
project1::display::Display::drop() & noexcept -> void
{
  m_running = false;
  if (m_window_class_atom != 0) {
    UnregisterClassW(m_window_class_name.data(), m_instance);
    m_window_class_atom = 0;
  }
}

auto
project1::display::Display::initialize() & noexcept -> project1::option::Option<project1::error::Win32Error>
{
  auto window_class = WNDCLASSEXW{
    sizeof(WNDCLASSEXW),
    /*CS_HREDRAW |*/ CS_OWNDC /*| CS_VREDRAW*/,
    window_procedure,
    0,
    0,
    m_instance,
    LoadIconW(nullptr, IDI_APPLICATION),
    LoadCursorW(nullptr, IDC_ARROW),
    nullptr,
    nullptr,
    m_window_class_name.data(),
    nullptr
    /*static_cast<HICON>(LoadImageW(m_instance,
				  MAKEINTRESOURCE(5),
				  IMAGE_ICON,
				  GetSystemMetrics(SM_CXSMICON),
				  GetSystemMetrics(SM_CYSMICON),
				  LR_DEFAULTCOLOR))*/
  };

  m_window_class_atom = RegisterClassExW(&window_class);
  if (m_window_class_atom == 0) {
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::last())));
  }

  return std::move(option::Option<error::Win32Error>::None());
}

auto
project1::display::Display::new_(HINSTANCE instance,
				 project1::option::Option<std::wstring_view> window_class_name) noexcept
  -> project1::result::Result<Display, error::Win32Error>
{
  using namespace std::literals;
  auto self =
    Display(instance, window_class_name.is_some() ? std::move(window_class_name).unwrap() : L"Project1Window"sv);
  if (auto result = self.initialize(); result.is_some()) {
    return std::move(result::Result<Display, error::Win32Error>::Err(std::move(std::move(result).unwrap())));
  }
  return std::move(result::Result<Display, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::display::Display::new_pointer(HINSTANCE instance,
					project1::option::Option<std::wstring_view> window_class_name) noexcept
  -> project1::result::Result<Display*, error::Win32Error>
{
  using namespace std::literals;
  auto* self =
    new Display(instance, window_class_name.is_some() ? std::move(window_class_name).unwrap() : L"Project1Window"sv);
  if (auto result = self->initialize(); result.is_some()) {
    delete self;
    return std::move(result::Result<Display*, error::Win32Error>::Err(std::move(std::move(result).unwrap())));
  }
  return std::move(result::Result<Display*, error::Win32Error>::Ok(self));
}

auto
project1::display::Display::run() & noexcept -> bool
{
  if (!m_running) {
    return m_running;
  }

  auto message = MSG{};
  while (PeekMessageW(&message, nullptr, 0, 0, PM_REMOVE) > 0) {
    if (message.message == WM_QUIT) {
      m_running = false;
    }
    TranslateMessage(&message);
    DispatchMessageW(&message);
  }

  return m_running;
}

LRESULT
project1::display::Display::window_procedure(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
  if (message == WM_NCCREATE) {
    auto* create_struct = reinterpret_cast<const CREATESTRUCTW*>(lParam);
    auto* data = reinterpret_cast<Window*>(create_struct->lpCreateParams);
    SetWindowLongPtrW(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(data));
    SetWindowLongPtrW(window, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(Window::window_procedure_proxy));
    return data->window_procedure(window, message, wParam, lParam);
  }
  return DefWindowProcW(window, message, wParam, lParam);
}
