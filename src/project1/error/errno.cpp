#include "project1/error/errno.hpp"

#include <cerrno>
#include <cstring>

auto
project1::error::operator<<(std::ostream& os, project1::error::Errno const& other) -> std::ostream&
{
  os << std::strerror(other.m_value);
  return os;
}

auto
project1::error::operator<<(std::wostream& os, project1::error::Errno const& other) -> std::wostream&
{
  auto converter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>();
  os << converter.from_bytes(std::strerror(other.m_value));
  return os;
}

auto
project1::error::Errno::last() noexcept -> project1::error::Errno
{
  return Errno(errno);
}
