#include "project1/error/win32_error.hpp"

#include <iomanip>
#include <sstream>

auto
project1::error::operator<<(std::wostream& os, project1::error::Win32Error const& self) -> std::wostream&
{
  os << self.message();
  return os;
}

project1::error::Win32Error
project1::error::Win32Error::last() noexcept
{
  return Win32Error(GetLastError());
}

auto
project1::error::Win32Error::message() const& noexcept -> std::wstring
{
  auto message = static_cast<LPWSTR>(nullptr);

  FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		 nullptr,
		 m_value,
		 MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		 reinterpret_cast<LPWSTR>(&message),
		 0,
		 nullptr);
  if (message == nullptr) {
    std::wostringstream out;
    out << L"Unknown error 0x" << std::hex << std::setfill(L'0') << std::setw(sizeof(HRESULT) * 2) << m_value;
    return out.str();
  }

  auto message_length = lstrlenW(message);
  if (message_length > 1 && message[message_length - 1] == L'\n') {
    message_length -= 1;
    if (message[message_length - 1] == L'\r') {
      message_length -= 1;
    }
  }
  auto result = std::wstring(std::wstring_view(message, message_length));
  LocalFree(message);

  return std::move(result);
}
