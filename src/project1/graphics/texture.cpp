#include "project1/graphics/texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

project1::graphics::Texture::Texture(project1::graphics::Texture&& other) noexcept
  : m_device(std::move(other.m_device))
  , m_device_context(std::move(other.m_device_context))
  , m_sampler_state(std::exchange(other.m_sampler_state, nullptr))
  , m_slot(std::move(other.m_slot))
  , m_texture_view(std::exchange(other.m_texture_view, nullptr))
{}

project1::graphics::Texture::~Texture() noexcept
{
  if (is_bound()) {
    unbind();
  }
  drop();
}

auto
project1::graphics::Texture::operator=(project1::graphics::Texture&& other) & noexcept -> project1::graphics::Texture&
{
  if (this == &other) {
    return *this;
  }

  if (is_bound()) {
    unbind();
  }
  drop();

  m_device = std::move(other.m_device);
  m_device_context = std::move(other.m_device_context);
  m_sampler_state = std::exchange(other.m_sampler_state, m_sampler_state);
  m_slot = std::move(other.m_slot);
  m_texture_view = std::exchange(other.m_texture_view, m_texture_view);

  return *this;
}

auto
project1::graphics::Texture::bind() const& noexcept -> void
{
  m_device_context->PSSetSamplers(m_slot, 1, &m_sampler_state);
  m_device_context->PSSetShaderResources(m_slot, 1, &m_texture_view);
}

auto
project1::graphics::Texture::drop() & noexcept -> void
{
  if (m_sampler_state != nullptr) {
    m_sampler_state->Release();
    m_sampler_state = nullptr;
  }

  if (m_texture_view != nullptr) {
    m_texture_view->Release();
    m_texture_view = nullptr;
  }
}

auto
project1::graphics::Texture::initialize(std::wstring_view texture_file_path) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  auto converter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>();

  auto channels = static_cast<int>(0);
  auto height = static_cast<int>(0);
  auto width = static_cast<int>(0);
  auto* pixels =
    stbi_load(converter.to_bytes(texture_file_path.data()).c_str(), &width, &height, &channels, STBI_rgb_alpha);

  auto texture_descriptor = D3D11_TEXTURE2D_DESC{
    static_cast<UINT>(width), static_cast<UINT>(height),  1, 1, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_SAMPLE_DESC{ 1, 0 },
    D3D11_USAGE_DEFAULT,      D3D11_BIND_SHADER_RESOURCE, 0, 0
  };
  auto texture_subresource_data = D3D11_SUBRESOURCE_DATA{ pixels, static_cast<UINT>(width * sizeof(stbi_uc) * 4), 0 };
  ID3D11Texture2D* texture = nullptr;
  if (auto result = m_device->CreateTexture2D(&texture_descriptor, &texture_subresource_data, &texture); result < 0) {
    stbi_image_free(pixels);
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }

  auto texture_view_descriptor =
    D3D11_SHADER_RESOURCE_VIEW_DESC{ texture_descriptor.Format, D3D11_SRV_DIMENSION_TEXTURE2D, 0, 1 };
  if (auto result = m_device->CreateShaderResourceView(texture, &texture_view_descriptor, &m_texture_view);
      result < 0) {
    stbi_image_free(pixels);
    texture->Release();
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }

  auto sampler_descriptor = D3D11_SAMPLER_DESC{ D3D11_FILTER_MIN_MAG_MIP_LINEAR,
						D3D11_TEXTURE_ADDRESS_CLAMP,
						D3D11_TEXTURE_ADDRESS_CLAMP,
						D3D11_TEXTURE_ADDRESS_CLAMP,
						0.0f,
						1,
						D3D11_COMPARISON_NEVER,
						{ 1.0f, 1.0f, 1.0f, 1.0f },
						-FLT_MAX,
						FLT_MAX };
  if (auto result = m_device->CreateSamplerState(&sampler_descriptor, &m_sampler_state); result < 0) {
    stbi_image_free(pixels);
    texture->Release();
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }

  stbi_image_free(pixels);
  texture->Release();
  return std::move(option::Option<error::Win32Error>::None());
}

auto
project1::graphics::Texture::is_bound() const& noexcept -> bool
{
  if (m_sampler_state == nullptr || m_texture_view == nullptr) {
    return false;
  }
  auto sampler_state = static_cast<ID3D11SamplerState*>(nullptr);
  m_device_context->PSGetSamplers(m_slot, 1, &sampler_state);
  auto texture_view = static_cast<ID3D11ShaderResourceView*>(nullptr);
  m_device_context->PSGetShaderResources(m_slot, 1, &texture_view);
  return m_sampler_state == sampler_state && m_texture_view == texture_view;
}

auto
project1::graphics::Texture::new_(ID3D11Device* device,
				  ID3D11DeviceContext* device_context,
				  UINT slot,
				  std::wstring_view texture_file_path) noexcept
  -> project1::result::Result<project1::graphics::Texture, project1::error::Win32Error>
{
  auto self = Texture(device, device_context, slot);
  if (auto result = self.initialize(texture_file_path); result.is_some()) {
    return std::move(result::Result<Texture, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Texture, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Texture::new_pointer(ID3D11Device* device,
					 ID3D11DeviceContext* device_context,
					 UINT slot,
					 std::wstring_view texture_file_path) noexcept
  -> project1::result::Result<project1::graphics::Texture*, project1::error::Win32Error>
{
  auto self = new Texture(device, device_context, slot);
  if (auto result = self->initialize(texture_file_path); result.is_some()) {
    delete self;
    return std::move(result::Result<Texture*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Texture*, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Texture::unbind() const& noexcept -> void
{
  static constexpr auto const sampler_state = static_cast<ID3D11SamplerState*>(nullptr);
  static constexpr auto const texture_view = static_cast<ID3D11ShaderResourceView*>(nullptr);
  m_device_context->PSSetSamplers(m_slot, 1, &sampler_state);
  m_device_context->PSSetShaderResources(m_slot, 1, &texture_view);
}
