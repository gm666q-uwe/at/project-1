#include "project1/graphics/buffer.hpp"

project1::graphics::Buffer::Buffer(project1::graphics::Buffer&& other) noexcept
  : m_buffer(std::exchange(other.m_buffer, nullptr))
  , m_device(std::move(other.m_device))
  , m_device_context(std::move(other.m_device_context))
{}

project1::graphics::Buffer::~Buffer() noexcept {
}

auto
project1::graphics::Buffer::operator=(project1::graphics::Buffer&& other) & noexcept -> project1::graphics::Buffer&
{
  if (this == &other) {
    return *this;
  }

  if (is_bound()) {
    unbind();
  }

  drop();

  m_buffer = std::exchange(other.m_buffer, m_buffer);
  m_device = std::move(other.m_device);
  m_device_context = std::move(other.m_device_context);

  return *this;
}

auto
project1::graphics::Buffer::drop() & noexcept -> void
{
  if (m_buffer != nullptr) {
    m_buffer->Release();
    m_buffer = nullptr;
  }
}

auto
project1::graphics::Buffer::initialize_base(UINT bind_flags,
					    UINT cpu_access_flags,
					    void const* data,
					    UINT size,
					    UINT stride,
					    D3D11_USAGE usage) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  auto buffer_descriptor = D3D11_BUFFER_DESC{ size, usage, bind_flags, cpu_access_flags, 0, stride };
  auto subresource_data = D3D11_SUBRESOURCE_DATA{ data, 0, 0 };
  if (auto result =
	m_device->CreateBuffer(&buffer_descriptor, data == nullptr ? nullptr : &subresource_data, &m_buffer);
      result < 0) {
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }

  return std::move(option::Option<error::Win32Error>::None());
}
