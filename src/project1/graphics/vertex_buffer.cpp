#include "project1/graphics/vertex_buffer.hpp"

project1::graphics::VertexBuffer::VertexBuffer(project1::graphics::VertexBuffer&& other) noexcept
  : Buffer(std::move(other))
{}

project1::graphics::VertexBuffer::~VertexBuffer() noexcept
{
  if (is_bound()) {
    unbind();
  }

  drop();
}

auto
project1::graphics::VertexBuffer::operator=(project1::graphics::VertexBuffer&& other) & noexcept
  -> project1::graphics::VertexBuffer&
{
  if (this == &Buffer::operator=(std::move(other))) {
    return *this;
  }

  return *this;
}

auto
project1::graphics::VertexBuffer::bind() const& noexcept -> void
{
  constexpr UINT const offset = 0;
  constexpr UINT const stride = sizeof(Vertex);
  m_device_context->IASetVertexBuffers(0, 1, &m_buffer, &stride, &offset);
}

auto
project1::graphics::VertexBuffer::initialize(std::span<const Vertex> vertices) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  constexpr const std::size_t stride = sizeof(Vertex);
  return std::move(initialize_base(D3D11_BIND_VERTEX_BUFFER,
				   0,
				   vertices.data(),
				   static_cast<UINT>(stride * vertices.size()),
				   stride,
				   D3D11_USAGE_DEFAULT));
}

auto
project1::graphics::VertexBuffer::is_bound() const& noexcept -> bool
{
  if (m_buffer == nullptr) {
    return false;
  }
  auto buffer = static_cast<ID3D11Buffer*>(nullptr);
  auto offsets = static_cast<UINT>(0);
  auto strides = static_cast<UINT>(0);
  m_device_context->IAGetVertexBuffers(0, 1, &buffer, &strides, &offsets);
  auto result = m_buffer == buffer;
  if (buffer != nullptr) {
    buffer->Release();
  }
  return result;
}

auto
project1::graphics::VertexBuffer::new_(ID3D11Device* device,
				       ID3D11DeviceContext* device_context,
				       std::span<const Vertex> vertices) noexcept
  -> project1::result::Result<project1::graphics::VertexBuffer, project1::error::Win32Error>
{
  auto self = VertexBuffer(device, device_context);
  if (auto result = self.initialize(vertices); result.is_some()) {
    return std::move(result::Result<VertexBuffer, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<VertexBuffer, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::VertexBuffer::new_pointer(ID3D11Device* device,
					      ID3D11DeviceContext* device_context,
					      std::span<const Vertex> vertices) noexcept
  -> project1::result::Result<project1::graphics::VertexBuffer*, project1::error::Win32Error>
{
  auto self = new VertexBuffer(device, device_context);
  if (auto result = self->initialize(vertices); result.is_some()) {
    delete self;
    return std::move(result::Result<VertexBuffer*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<VertexBuffer*, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::VertexBuffer::unbind() const& noexcept -> void
{
  static constexpr auto const buffer = static_cast<ID3D11Buffer*>(nullptr);
  m_device_context->IASetVertexBuffers(0, 1, &buffer, nullptr, nullptr);
}
