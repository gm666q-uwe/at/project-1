#include "project1/graphics/index_buffer.hpp"

project1::graphics::IndexBuffer::IndexBuffer(project1::graphics::IndexBuffer&& other) noexcept
  : Buffer(std::move(other))
  , m_size(std::move(other.m_size))
{}

project1::graphics::IndexBuffer::~IndexBuffer() noexcept
{
  if (is_bound()) {
    unbind();
  }

  drop();
}

auto
project1::graphics::IndexBuffer::operator=(project1::graphics::IndexBuffer&& other) & noexcept
  -> project1::graphics::IndexBuffer&
{
  if (this == &Buffer::operator=(std::move(other))) {
    return *this;
  }

  m_size = other.m_size;

  return *this;
}

auto
project1::graphics::IndexBuffer::bind() const& noexcept -> void
{
  m_device_context->IASetIndexBuffer(m_buffer, DXGI_FORMAT_R16_UINT, 0);
}

auto
project1::graphics::IndexBuffer::initialize(std::span<const std::uint16_t> indices) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  constexpr std::size_t const stride = sizeof(std::uint16_t);
  return std::move(initialize_base(D3D11_BIND_INDEX_BUFFER,
				   0,
				   indices.data(),
				   static_cast<UINT>(stride * indices.size()),
				   stride,
				   D3D11_USAGE_DEFAULT));
}

auto
project1::graphics::IndexBuffer::is_bound() const& noexcept -> bool
{
  if (m_buffer == nullptr) {
    return false;
  }
  auto* buffer = static_cast<ID3D11Buffer*>(nullptr);
  auto format = DXGI_FORMAT_UNKNOWN;
  auto offset = static_cast<UINT>(0);
  m_device_context->IAGetIndexBuffer(&buffer, &format, &offset);
  auto result = m_buffer == buffer;
  if(buffer != nullptr) {
    buffer->Release();
  }
  return result;
}

auto
project1::graphics::IndexBuffer::new_(ID3D11Device* device,
				      ID3D11DeviceContext* device_context,
				      std::span<const std::uint16_t> indices) noexcept
  -> project1::result::Result<project1::graphics::IndexBuffer, project1::error::Win32Error>
{
  auto self = IndexBuffer(device, device_context, indices.size());
  if (auto result = self.initialize(indices); result.is_some()) {
    return std::move(result::Result<IndexBuffer, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<IndexBuffer, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::IndexBuffer::new_pointer(ID3D11Device* device,
					     ID3D11DeviceContext* device_context,
					     std::span<const std::uint16_t> indices) noexcept
  -> project1::result::Result<project1::graphics::IndexBuffer*, project1::error::Win32Error>
{
  auto self = new IndexBuffer(device, device_context, indices.size());
  if (auto result = self->initialize(indices); result.is_some()) {
    delete self;
    return std::move(result::Result<IndexBuffer*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<IndexBuffer*, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::IndexBuffer::size() const& noexcept -> std::size_t
{
  return m_size;
}

auto
project1::graphics::IndexBuffer::unbind() const& noexcept -> void
{
  m_device_context->IASetIndexBuffer(nullptr, DXGI_FORMAT_R16_UINT, 0);
}
