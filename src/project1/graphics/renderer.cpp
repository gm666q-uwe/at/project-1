#include "project1/graphics/renderer.hpp"

project1::graphics::Renderer::Renderer(project1::graphics::Renderer&& other) noexcept
  : m_depth_stencil_view(std::exchange(other.m_depth_stencil_view, nullptr))
  , m_device(std::exchange(other.m_device, nullptr))
  , m_device_context(std::exchange(other.m_device_context, nullptr))
  , m_render_target_view(std::exchange(other.m_render_target_view, nullptr))
  , m_swap_chain(std::exchange(other.m_swap_chain, nullptr))
{}

project1::graphics::Renderer::~Renderer() noexcept
{
  drop();
}

auto
project1::graphics::Renderer::operator=(project1::graphics::Renderer&& other) & noexcept
  -> project1::graphics::Renderer&
{
  if (this == &other) {
    return *this;
  }

  drop();

  m_depth_stencil_view = std::exchange(other.m_depth_stencil_view, m_depth_stencil_view);
  m_device = std::exchange(other.m_device, m_device);
  m_device_context = std::exchange(other.m_device_context, m_device_context);
  m_render_target_view = std::exchange(other.m_render_target_view, m_render_target_view);
  m_swap_chain = std::exchange(other.m_swap_chain, m_swap_chain);

  return *this;
}

auto
project1::graphics::Renderer::begin_frame() const& noexcept -> void
{}

auto
project1::graphics::Renderer::clear(DirectX::XMVECTORF32 const& color) const& noexcept -> void
{
  clear_depth();
  clear_color(color);
}

auto
project1::graphics::Renderer::clear_color(DirectX::XMVECTORF32 const& color) const& noexcept -> void
{
  m_device_context->ClearRenderTargetView(m_render_target_view, color);
}

auto
project1::graphics::Renderer::clear_depth() const& noexcept -> void
{
  m_device_context->ClearDepthStencilView(m_depth_stencil_view, D3D11_CLEAR_DEPTH, 1.0, 0);
}

auto
project1::graphics::Renderer::create_cube() const& noexcept
  -> project1::result::Result<project1::graphics::mesh::Cube, project1::error::Win32Error>
{
  return mesh::Cube::new_(m_device, m_device_context);
}

auto
project1::graphics::Renderer::create_cube_pointer() const& noexcept
  -> project1::result::Result<project1::graphics::mesh::Cube*, project1::error::Win32Error>
{
  return mesh::Cube::new_pointer(m_device, m_device_context);
}

auto
project1::graphics::Renderer::create_index_buffer(std::span<const std::uint16_t> indices) const& noexcept
  -> project1::result::Result<project1::graphics::IndexBuffer, project1::error::Win32Error>
{
  return IndexBuffer::new_(m_device, m_device_context, indices);
}

auto
project1::graphics::Renderer::create_index_buffer_pointer(std::span<const std::uint16_t> indices) const& noexcept
  -> project1::result::Result<project1::graphics::IndexBuffer*, project1::error::Win32Error>
{
  return IndexBuffer::new_pointer(m_device, m_device_context, indices);
}

auto
project1::graphics::Renderer::create_shader(std::wstring_view vertex_shader_file_path,
					    std::wstring_view pixel_shader_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::Shader, project1::error::Win32Error>
{
  return Shader::new_(m_device, m_device_context, vertex_shader_file_path, pixel_shader_file_path);
}

auto
project1::graphics::Renderer::create_shader_pointer(std::wstring_view vertex_shader_file_path,
						    std::wstring_view pixel_shader_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::Shader*, project1::error::Win32Error>
{
  return Shader::new_pointer(m_device, m_device_context, vertex_shader_file_path, pixel_shader_file_path);
}

auto
project1::graphics::Renderer::create_texture(UINT slot, std::wstring_view texture_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::Texture, project1::error::Win32Error>
{
  return Texture::new_(m_device, m_device_context, slot, texture_file_path);
}

auto
project1::graphics::Renderer::create_texture_pointer(UINT slot, std::wstring_view texture_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::Texture*, project1::error::Win32Error>
{
  return Texture::new_pointer(m_device, m_device_context, slot, texture_file_path);
}

auto
project1::graphics::Renderer::create_vertex_buffer(std::span<const Vertex> vertices) const& noexcept
  -> project1::result::Result<project1::graphics::VertexBuffer, project1::error::Win32Error>
{
  return VertexBuffer::new_(m_device, m_device_context, vertices);
}

auto
project1::graphics::Renderer::create_vertex_buffer_pointer(std::span<const Vertex> vertices) const& noexcept
  -> project1::result::Result<project1::graphics::VertexBuffer*, project1::error::Win32Error>
{
  return VertexBuffer::new_pointer(m_device, m_device_context, vertices);
}

auto
project1::graphics::Renderer::drop() & noexcept -> void
{
  if (m_render_target_view != nullptr) {
    m_render_target_view->Release();
    m_render_target_view = nullptr;
  }

  if (m_depth_stencil_view != nullptr) {
    m_depth_stencil_view->Release();
    m_depth_stencil_view = nullptr;
  }

  if (m_swap_chain != nullptr) {
    m_swap_chain->Release();
    m_swap_chain = nullptr;
  }

  if (m_device_context != nullptr) {
    m_device_context->Release();
    m_device_context = nullptr;
  }

  if (m_device != nullptr) {
    m_device->Release();
    m_device = nullptr;
  }
}

auto
project1::graphics::Renderer::end_frame() const& noexcept -> void
{
  // TODO: Maybe check error
  m_swap_chain->Present(0, 0);
}

auto
project1::graphics::Renderer::initialize(UINT width, UINT height, HWND window) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  auto swap_chain_descriptor = DXGI_SWAP_CHAIN_DESC{ DXGI_MODE_DESC{ width,
								     height,
								     DXGI_RATIONAL{ 60, 1 },
								     DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
								     DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
								     DXGI_MODE_SCALING_UNSPECIFIED },
						     DXGI_SAMPLE_DESC{ 1, 0 },
						     DXGI_USAGE_RENDER_TARGET_OUTPUT,
						     1,
						     window,
						     TRUE,
						     DXGI_SWAP_EFFECT_DISCARD,
						     0 };
  if (auto result = D3D11CreateDeviceAndSwapChain(nullptr,
						  D3D_DRIVER_TYPE_HARDWARE,
						  nullptr,
						  0,
						  nullptr,
						  0,
						  D3D11_SDK_VERSION,
						  &swap_chain_descriptor,
						  &m_swap_chain,
						  &m_device,
						  nullptr,
						  &m_device_context);
      result < 0) {
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }

  ID3D11Resource* back_buffer = nullptr;
  if (auto result = m_swap_chain->GetBuffer(0, __uuidof(ID3D11Resource), reinterpret_cast<void**>(&back_buffer));
      result < 0) {
    if (back_buffer != nullptr) {
      back_buffer->Release();
    }
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }
  if (auto result = m_device->CreateRenderTargetView(back_buffer, nullptr, &m_render_target_view); result < 0) {
    back_buffer->Release();
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }
  back_buffer->Release();

  auto depth_stencil_descriptor = D3D11_DEPTH_STENCIL_DESC{ TRUE,
							    D3D11_DEPTH_WRITE_MASK_ALL,
							    D3D11_COMPARISON_LESS,
							    FALSE,
							    D3D11_DEFAULT_STENCIL_READ_MASK,
							    D3D11_DEFAULT_STENCIL_WRITE_MASK,
							    D3D11_DEPTH_STENCILOP_DESC{ D3D11_STENCIL_OP_KEEP,
											D3D11_STENCIL_OP_KEEP,
											D3D11_STENCIL_OP_KEEP,
											D3D11_COMPARISON_ALWAYS },
							    D3D11_DEPTH_STENCILOP_DESC{
							      D3D11_STENCIL_OP_KEEP,
							      D3D11_STENCIL_OP_KEEP,
							      D3D11_STENCIL_OP_KEEP,
							      D3D11_COMPARISON_ALWAYS,
							    } };
  ID3D11DepthStencilState* depth_stencil_state = nullptr;
  if (auto result = m_device->CreateDepthStencilState(&depth_stencil_descriptor, &depth_stencil_state); result < 0) {
    if (depth_stencil_state != nullptr) {
      depth_stencil_state->Release();
    }
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }
  m_device_context->OMSetDepthStencilState(depth_stencil_state, 1);
  depth_stencil_state->Release();

  ID3D11Texture2D* depth_stencil_texture = nullptr;
  auto depth_stencil_texture_descriptor = D3D11_TEXTURE2D_DESC{
    width, height, 1, 1, DXGI_FORMAT_D32_FLOAT, DXGI_SAMPLE_DESC{ 1, 0 }, D3D11_USAGE_DEFAULT, D3D11_BIND_DEPTH_STENCIL,
    0,	   0
  };
  if (auto result = m_device->CreateTexture2D(&depth_stencil_texture_descriptor, nullptr, &depth_stencil_texture);
      result < 0) {
    if (depth_stencil_texture != nullptr) {
      depth_stencil_texture->Release();
    }
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }
  auto depth_stencil_view_descriptor =
    D3D11_DEPTH_STENCIL_VIEW_DESC{ DXGI_FORMAT_D32_FLOAT, D3D11_DSV_DIMENSION_TEXTURE2D, 0, 0 };
  if (auto result =
	m_device->CreateDepthStencilView(depth_stencil_texture, &depth_stencil_view_descriptor, &m_depth_stencil_view);
      result < 0) {
    depth_stencil_texture->Release();
    return std::move(option::Option<error::Win32Error>::Some(std::move(error::Win32Error::new_(result))));
  }
  depth_stencil_texture->Release();

  m_device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  m_device_context->OMSetRenderTargets(1, &m_render_target_view, m_depth_stencil_view);
  auto viewport = D3D11_VIEWPORT{ 0.0f, 0.0f, static_cast<FLOAT>(width), static_cast<FLOAT>(height), 0.0f, 1.0f };
  m_device_context->RSSetViewports(1, &viewport);

  m_perspective_matrix = DirectX::XMMatrixPerspectiveFovLH(
    DirectX::XMConvertToRadians(60.0f), static_cast<float>(width) / static_cast<float>(height), 0.3f, 1000.0f);

  return std::move(option::Option<error::Win32Error>::None());
}

auto
project1::graphics::Renderer::load_triangle_mesh_obj(std::wstring_view mesh_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::mesh::TriangleMesh, project1::error::Win32Error>
{
  return std::move(mesh::TriangleMesh::load_obj(m_device, m_device_context, mesh_file_path));
}

auto
project1::graphics::Renderer::load_triangle_mesh_obj_pointer(std::wstring_view mesh_file_path) const& noexcept
  -> project1::result::Result<project1::graphics::mesh::TriangleMesh*, project1::error::Win32Error>
{
  return std::move(mesh::TriangleMesh::load_obj_pointer(m_device, m_device_context, mesh_file_path));
}

auto
project1::graphics::Renderer::new_(UINT width, UINT height, HWND window) noexcept
  -> project1::result::Result<project1::graphics::Renderer, project1::error::Win32Error>
{
  auto self = Renderer();
  if (auto result = self.initialize(width, height, window); result.is_some()) {
    return std::move(result::Result<Renderer, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Renderer, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Renderer::new_pointer(UINT width, UINT height, HWND window) noexcept
  -> project1::result::Result<project1::graphics::Renderer*, project1::error::Win32Error>
{
  auto self = new Renderer();
  if (auto result = self->initialize(width, height, window); result.has_value()) {
    delete self;
    return std::move(result::Result<Renderer*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Renderer*, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Renderer::perspective_matrix() const& noexcept -> DirectX::XMMATRIX const&
{
  return m_perspective_matrix;
}
