#include "project1/graphics/mesh/cube.hpp"

project1::graphics::mesh::Cube::Cube(ID3D11Device* device,
				     ID3D11DeviceContext* device_context,
				     project1::graphics::IndexBuffer&& index_buffer,
				     project1::graphics::VertexBuffer&& vertex_buffer) noexcept
  : m_device(device)
  , m_device_context(device_context)
  , m_index_buffer(std::move(index_buffer))
  , m_vertex_buffer(std::move(vertex_buffer))
{}

project1::graphics::mesh::Cube::Cube(project1::graphics::mesh::Cube&& other) noexcept
  : m_device(std::move(other.m_device))
  , m_device_context(std::move(other.m_device_context))
  , m_index_buffer(std::move(other.m_index_buffer))
  , m_vertex_buffer(std::move(other.m_vertex_buffer))
{}

project1::graphics::mesh::Cube::~Cube() noexcept {}

auto
project1::graphics::mesh::Cube::operator=(project1::graphics::mesh::Cube&& other) & noexcept
  -> project1::graphics::mesh::Cube&
{
  if (this == &other) {
    return *this;
  }

  m_device = std::move(other.m_device);
  m_device_context = std::move(other.m_device_context);
  m_index_buffer = std::move(other.m_index_buffer);
  m_vertex_buffer = std::move(other.m_vertex_buffer);

  return *this;
}

auto
project1::graphics::mesh::Cube::bind() const& noexcept -> void
{
  m_index_buffer.bind();
  m_vertex_buffer.bind();
}

auto
project1::graphics::mesh::Cube::draw() const& noexcept -> void
{
  if (!is_bound()) {
    bind();
  }
  // TODO: Maybe check error
  m_device_context->DrawIndexed(m_index_buffer.size(), 0, 0);
}

auto
project1::graphics::mesh::Cube::is_bound() const& noexcept -> bool
{
  return m_index_buffer.is_bound() && m_vertex_buffer.is_bound();
}

auto
project1::graphics::mesh::Cube::new_(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
  -> project1::result::Result<project1::graphics::mesh::Cube, project1::error::Win32Error>
{
  auto index_buffer = IndexBuffer::new_(device, device_context, M_INDICES);
  if (index_buffer.is_err()) {
    return std::move(result::Result<Cube, error::Win32Error>::Err(std::move(index_buffer).unwrap_err()));
  }
  auto vertex_buffer = VertexBuffer::new_(device, device_context, M_VERTICES);
  if (vertex_buffer.is_err()) {
    return std::move(result::Result<Cube, error::Win32Error>::Err(std::move(vertex_buffer).unwrap_err()));
  }
  return std::move(result::Result<Cube, error::Win32Error>::Ok(
    Cube(device, device_context, std::move(index_buffer).unwrap(), std::move(vertex_buffer).unwrap())));
}

auto
project1::graphics::mesh::Cube::new_pointer(ID3D11Device* device, ID3D11DeviceContext* device_context) noexcept
  -> project1::result::Result<project1::graphics::mesh::Cube*, project1::error::Win32Error>
{
  auto index_buffer = IndexBuffer::new_(device, device_context, M_INDICES);
  if (index_buffer.is_err()) {
    return std::move(result::Result<Cube*, error::Win32Error>::Err(std::move(index_buffer).unwrap_err()));
  }
  auto vertex_buffer = VertexBuffer::new_(device, device_context, M_VERTICES);
  if (vertex_buffer.is_err()) {
    return std::move(result::Result<Cube*, error::Win32Error>::Err(std::move(vertex_buffer).unwrap_err()));
  }
  return std::move(result::Result<Cube*, error::Win32Error>::Ok(
    new Cube(device, device_context, std::move(index_buffer).unwrap(), std::move(vertex_buffer).unwrap())));
}

auto
project1::graphics::mesh::Cube::unbind() const& noexcept -> void
{
  m_index_buffer.unbind();
  m_vertex_buffer.unbind();
}
