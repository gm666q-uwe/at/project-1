#include "project1/graphics/mesh/triangle_mesh.hpp"

#include <algorithm>
#include <iostream>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

project1::graphics::mesh::TriangleMesh::TriangleMesh(ID3D11Device* device,
						     ID3D11DeviceContext* device_context,
						     project1::graphics::IndexBuffer&& index_buffer,
						     project1::graphics::VertexBuffer&& vertex_buffer) noexcept
  : m_device(device)
  , m_device_context(device_context)
  , m_index_buffer(std::move(index_buffer))
  , m_vertex_buffer(std::move(vertex_buffer))
{}

project1::graphics::mesh::TriangleMesh::TriangleMesh(project1::graphics::mesh::TriangleMesh&& other) noexcept
  : m_device(std::move(other.m_device))
  , m_device_context(std::move(other.m_device_context))
  , m_index_buffer(std::move(other.m_index_buffer))
  , m_vertex_buffer(std::move(other.m_vertex_buffer))
{}

project1::graphics::mesh::TriangleMesh::~TriangleMesh() noexcept {}

auto
project1::graphics::mesh::TriangleMesh::operator=(project1::graphics::mesh::TriangleMesh&& other) & noexcept
  -> project1::graphics::mesh::TriangleMesh&
{
  if (this == &other) {
    return *this;
  }

  m_device = std::move(other.m_device);
  m_device_context = std::move(other.m_device_context);
  m_index_buffer = std::move(other.m_index_buffer);
  m_vertex_buffer = std::move(other.m_vertex_buffer);

  return *this;
}

auto
project1::graphics::mesh::TriangleMesh::bind() const& noexcept -> void
{
  m_index_buffer.bind();
  m_vertex_buffer.bind();
}

auto
project1::graphics::mesh::TriangleMesh::draw() const& noexcept -> void
{
  if (!is_bound()) {
    bind();
  }
  // TODO: Maybe check error
  m_device_context->DrawIndexed(m_index_buffer.size(), 0, 0);
}

auto
project1::graphics::mesh::TriangleMesh::initialize_obj(std::wstring_view mesh_file_path,
						       std::vector<std::uint16_t>& indices,
						       std::vector<Vertex>& vertices) noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  auto reader_config = tinyobj::ObjReaderConfig();
  auto reader = tinyobj::ObjReader();
  // TODO: Check for IO errors.
  auto converter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>();
  reader.ParseFromFile(converter.to_bytes(mesh_file_path.data()));

  if (!reader.Warning().empty()) {
    std::cout << "TinyObjReader: " << reader.Warning() << std::endl;
  }

  auto const& attrib = reader.GetAttrib();
  auto const& shape = reader.GetShapes()[0];
  indices.reserve(shape.mesh.indices.size());
  // TODO: Kinda wasteful
  vertices.reserve(shape.mesh.indices.size());
  for (auto const& index : shape.mesh.indices) {
    auto vertex = Vertex{ { attrib.vertices[3 * index.vertex_index + 0],
			    attrib.vertices[3 * index.vertex_index + 1],
			    attrib.vertices[3 * index.vertex_index + 2] },
			  {},
			  {} };
    if (index.texcoord_index >= 0) {
      vertex.uv = { attrib.texcoords[2 * index.texcoord_index + 0], attrib.texcoords[2 * index.texcoord_index + 1] };
    }
    if (index.normal_index >= 0) {
      vertex.normal = { attrib.normals[3 * index.normal_index + 0],
			attrib.normals[3 * index.normal_index + 1],
			attrib.normals[3 * index.normal_index + 2] };
    }

    std::size_t i = 0;
    if (auto const& it = std::find(vertices.begin(), vertices.end(), vertex); it == vertices.end()) {
      i = vertices.size();
      vertices.push_back(vertex);
    } else {
      i = std::distance(vertices.begin(), it);
    }
    indices.push_back(i);
  }

  return option::Option<error::Win32Error>::None();
}

auto
project1::graphics::mesh::TriangleMesh::is_bound() const& noexcept -> bool
{
  return m_index_buffer.is_bound() && m_vertex_buffer.is_bound();
}

auto
project1::graphics::mesh::TriangleMesh::load_obj(ID3D11Device* device,
						 ID3D11DeviceContext* device_context,
						 std::wstring_view mesh_file_path) noexcept
  -> project1::result::Result<project1::graphics::mesh::TriangleMesh, project1::error::Win32Error>
{
  auto indices = std::vector<std::uint16_t>();
  auto vertices = std::vector<Vertex>();
  if (auto result = initialize_obj(mesh_file_path, indices, vertices); result.is_some()) {
    return std::move(result::Result<TriangleMesh, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  auto index_buffer = IndexBuffer::new_(device, device_context, indices);
  if (index_buffer.is_err()) {
    return std::move(result::Result<TriangleMesh, error::Win32Error>::Err(std::move(index_buffer).unwrap_err()));
  }
  auto vertex_buffer = VertexBuffer::new_(device, device_context, vertices);
  if (vertex_buffer.is_err()) {
    return std::move(result::Result<TriangleMesh, error::Win32Error>::Err(std::move(vertex_buffer).unwrap_err()));
  }
  return std::move(result::Result<TriangleMesh, error::Win32Error>::Ok(std::move(
    TriangleMesh(device, device_context, std::move(index_buffer).unwrap(), std::move(vertex_buffer).unwrap()))));
}

auto
project1::graphics::mesh::TriangleMesh::load_obj_pointer(ID3D11Device* device,
							 ID3D11DeviceContext* device_context,
							 std::wstring_view mesh_file_path) noexcept
  -> project1::result::Result<project1::graphics::mesh::TriangleMesh*, project1::error::Win32Error>
{
  auto indices = std::vector<std::uint16_t>();
  auto vertices = std::vector<Vertex>();
  if (auto result = initialize_obj(mesh_file_path, indices, vertices); result.is_some()) {
    return std::move(result::Result<TriangleMesh*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  auto index_buffer = IndexBuffer::new_(device, device_context, indices);
  if (index_buffer.is_err()) {
    return std::move(result::Result<TriangleMesh*, error::Win32Error>::Err(std::move(index_buffer).unwrap_err()));
  }
  auto vertex_buffer = VertexBuffer::new_(device, device_context, vertices);
  if (vertex_buffer.is_err()) {
    return std::move(result::Result<TriangleMesh*, error::Win32Error>::Err(std::move(vertex_buffer).unwrap_err()));
  }
  return std::move(result::Result<TriangleMesh*, error::Win32Error>::Ok(std::move(
    new TriangleMesh(device, device_context, std::move(index_buffer).unwrap(), std::move(vertex_buffer).unwrap()))));
}

auto
project1::graphics::mesh::TriangleMesh::unbind() const& noexcept -> void
{
  m_index_buffer.unbind();
  m_vertex_buffer.unbind();
}
