#include "project1/graphics/shader.hpp"

#include <array>
#include <fstream>

project1::graphics::Shader::Shader(project1::graphics::Shader&& other) noexcept
  : m_device(std::move(other.m_device))
  , m_device_context(std::move(other.m_device_context))
  , m_input_layout(std::exchange(other.m_input_layout, nullptr))
  , m_pixel_shader(std::exchange(other.m_pixel_shader, nullptr))
  , m_vertex_shader(std::exchange(other.m_vertex_shader, nullptr))
{}

project1::graphics::Shader::~Shader() noexcept
{
  if (is_bound()) {
    unbind();
  }
  drop();
}

auto
project1::graphics::Shader::operator=(project1::graphics::Shader&& other) & noexcept -> project1::graphics::Shader&
{
  if (this == &other) {
    return *this;
  }

  if (is_bound()) {
    unbind();
  }
  drop();

  m_device = std::move(other.m_device);
  m_device_context = std::move(other.m_device_context);
  m_input_layout = std::exchange(other.m_input_layout, m_input_layout);
  m_pixel_shader = std::exchange(other.m_pixel_shader, m_pixel_shader);
  m_vertex_shader = std::exchange(other.m_vertex_shader, m_vertex_shader);

  return *this;
}

void
project1::graphics::Shader::bind() const& noexcept
{
  m_device_context->IASetInputLayout(m_input_layout);
  m_device_context->PSSetShader(m_pixel_shader, nullptr, 0);
  m_device_context->VSSetShader(m_vertex_shader, nullptr, 0);
}

auto
project1::graphics::Shader::compile(std::wstring_view shader_file_path, Type type) const& noexcept
  -> project1::result::Result<ID3DBlob*, project1::error::Win32Error>
{
  // D3DCompileFromFile doesn't work for some reason.
  // TODO: Check for IO errors.
  auto shader_file = std::ifstream(shader_file_path.data(), std::ios::binary);
  auto buffer = std::vector<char>(std::istreambuf_iterator<char>(shader_file), std::istreambuf_iterator<char>());

  ID3DBlob* blob = nullptr;
  auto converter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>();
  if (auto result = D3DCompile(buffer.data(),
			       buffer.size(),
			       converter.to_bytes(shader_file_path.data()).c_str(),
			       nullptr,
			       nullptr,
			       "main",
			       M_TYPE_TARGETS[static_cast<std::size_t>(type)],
			       0,
			       0,
			       &blob,
			       nullptr);
      result < 0) {
    return std::move(result::Result<ID3DBlob*, error::Win32Error>::Err(error::Win32Error::new_(result)));
  }

  return std::move(result::Result<ID3DBlob*, error::Win32Error>::Ok(std::move(blob)));
}

auto
project1::graphics::Shader::drop() & noexcept -> void
{
  if (m_input_layout != nullptr) {
    m_input_layout->Release();
    m_input_layout = nullptr;
  }

  if (m_pixel_shader != nullptr) {
    m_pixel_shader->Release();
    m_pixel_shader = nullptr;
  }

  if (m_vertex_shader != nullptr) {
    m_vertex_shader->Release();
    m_vertex_shader = nullptr;
  }
}

auto
project1::graphics::Shader::new_(ID3D11Device* device,
				 ID3D11DeviceContext* device_context,
				 std::wstring_view vertex_shader_file_path,
				 std::wstring_view pixel_shader_file_path) noexcept
  -> project1::result::Result<project1::graphics::Shader, project1::error::Win32Error>
{
  auto self = Shader(device, device_context);
  if (auto result = self.initialize(vertex_shader_file_path, pixel_shader_file_path); result.is_some()) {
    return std::move(result::Result<Shader, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Shader, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Shader::new_pointer(ID3D11Device* device,
					ID3D11DeviceContext* device_context,
					std::wstring_view vertex_shader_file_path,
					std::wstring_view pixel_shader_file_path) noexcept
  -> project1::result::Result<project1::graphics::Shader*, project1::error::Win32Error>
{
  auto self = new Shader(device, device_context);
  if (auto result = self->initialize(vertex_shader_file_path, pixel_shader_file_path); result.is_some()) {
    delete self;
    return std::move(result::Result<Shader*, error::Win32Error>::Err(std::move(result).unwrap()));
  }
  return std::move(result::Result<Shader*, error::Win32Error>::Ok(std::move(self)));
}

auto
project1::graphics::Shader::initialize(std::wstring_view vertex_shader_file_path,
				       std::wstring_view pixel_shader_file_path) & noexcept
  -> project1::option::Option<project1::error::Win32Error>
{
  ID3DBlob* pixel_blob;
  if (auto result = compile(pixel_shader_file_path, Type::Pixel); result.is_err()) {
    return std::move(option::Option<error::Win32Error>::Some(std::move(result).unwrap_err()));
  } else {
    pixel_blob = std::move(result).unwrap();
  }
  ID3DBlob* vertex_blob;
  if (auto result = compile(vertex_shader_file_path, Type::Vertex); result.is_err()) {
    return std::move(option::Option<error::Win32Error>::Some(std::move(result).unwrap_err()));
  } else {
    vertex_blob = std::move(result).unwrap();
  }

  if (auto result = m_device->CreatePixelShader(
	pixel_blob->GetBufferPointer(), pixel_blob->GetBufferSize(), nullptr, &m_pixel_shader);
      result < 0) {
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }
  if (auto result = m_device->CreateVertexShader(
	vertex_blob->GetBufferPointer(), vertex_blob->GetBufferSize(), nullptr, &m_vertex_shader);
      result < 0) {
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }

  constexpr auto const input_element_descriptors = std::array<D3D11_INPUT_ELEMENT_DESC, 3>{
    D3D11_INPUT_ELEMENT_DESC{ "Position", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    D3D11_INPUT_ELEMENT_DESC{
      "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    D3D11_INPUT_ELEMENT_DESC{
      "Normal", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
  };
  if (auto result = m_device->CreateInputLayout(input_element_descriptors.data(),
						input_element_descriptors.size(),
						vertex_blob->GetBufferPointer(),
						vertex_blob->GetBufferSize(),
						&m_input_layout);
      result < 0) {
    return std::move(option::Option<error::Win32Error>::Some(error::Win32Error::new_(result)));
  }

  return std::move(option::Option<error::Win32Error>::None());
}

auto
project1::graphics::Shader::is_bound() const& noexcept -> bool
{
  if (m_input_layout == nullptr && m_pixel_shader == nullptr && m_vertex_shader == nullptr) {
    return false;
  }
  ID3D11InputLayout* input_layout = nullptr;
  m_device_context->IAGetInputLayout(&input_layout);
  ID3D11PixelShader* pixel_shader = nullptr;
  m_device_context->PSGetShader(&pixel_shader, nullptr, nullptr);
  ID3D11VertexShader* vertex_shader = nullptr;
  m_device_context->VSGetShader(&vertex_shader, nullptr, nullptr);
  return m_input_layout == input_layout && pixel_shader == m_pixel_shader && vertex_shader == m_vertex_shader;
}

auto
project1::graphics::Shader::unbind() const& noexcept -> void
{
  m_device_context->IASetInputLayout(nullptr);
  m_device_context->PSSetShader(nullptr, nullptr, 0);
  m_device_context->VSSetShader(nullptr, nullptr, 0);
}
