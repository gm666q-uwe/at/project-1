#include "project1/project1.hpp"

int WINAPI
wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nShowCmd)
{
  auto app = project1::Project1::new_();
  return app.run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
}
