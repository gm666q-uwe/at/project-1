#include "project1/app.hpp"

#include <algorithm>
#include <chrono>

auto
project1::App::run(std::span<std::wstring_view> args) & noexcept -> int
{
  init(args);

  auto accumulator = 0.0;
  auto current_time = std::chrono::high_resolution_clock::now();
  auto fixed_time = 0.0;
  auto time = 0.0;

  while (running()) {
    auto new_time = std::chrono::high_resolution_clock::now();
    auto delta_time = std::chrono::duration<double>(new_time - current_time).count();
    accumulator += delta_time;
    current_time = new_time;

    // Input
    input();

    // Update
    while (accumulator >= TARGET_DELTA_TIME) {
      fixed_update(TARGET_DELTA_TIME, fixed_time);
      accumulator -= TARGET_DELTA_TIME;
      fixed_time += TARGET_DELTA_TIME;
    }
    update(delta_time, time);
    late_update(delta_time, time);

    // Render
    render(time);

    time += delta_time;
  }

  return result();
}

auto
project1::App::run(int argc, char const** argv) & noexcept -> int
{
  auto args_ = std::span(argv, argc);
  auto args = std::vector<std::wstring>();
  auto converter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>();
  std::transform(
    args_.begin(), args_.end(), std::back_inserter(args), [&converter](auto const* arg) -> auto {
      return converter.from_bytes(arg);
    });

  auto arg_views = std::vector<std::wstring_view>();
  std::transform(
    args.begin(), args.end(), std::back_inserter(arg_views), [](auto const& arg) -> auto {
      return std::wstring_view(arg);
    });

  return run(arg_views);
}

auto
project1::App::run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int
{
  int argc = 0;
  auto* argv = CommandLineToArgvW(lpCmdLine, &argc);
  auto args = std::span(argv, argc);

  auto arg_views = std::vector<std::wstring_view>();
  std::transform(
    args.begin(), args.end(), std::back_inserter(arg_views), [](auto const* arg) -> auto {
      return std::wstring_view(arg);
    });

  return run(arg_views);
}
