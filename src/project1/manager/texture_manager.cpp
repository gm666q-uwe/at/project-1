#include "project1/manager/texture_manager.hpp"

#include <sstream>

template<>
std::unique_ptr<project1::manager::TextureManager>
  project1::utility::Singleton<project1::manager::TextureManager>::m_instance = nullptr;

template<>
std::mutex project1::utility::Singleton<project1::manager::TextureManager>::m_instance_mutex = std::mutex();

project1::manager::TextureManager::TextureManager(project1::graphics::Renderer* renderer) noexcept
  : project1::utility::Singleton<project1::manager::TextureManager>()
  , m_renderer(renderer)
{}

auto
project1::manager::TextureManager::index(std::wstring const& name, UINT slot) & noexcept
  -> project1::result::Result<size_t, project1::error::Win32Error>
{
  auto it = m_texture_names.find(name);
  if (it == m_texture_names.end()) {
    return std::move(load(name, slot));
  }
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(it->second));
}

auto
project1::manager::TextureManager::load(std::wstring const& name, UINT slot) & noexcept
  -> project1::result::Result<std::size_t, project1::error::Win32Error>
{
  auto texture_file_path = std::wostringstream();
  texture_file_path << L"data/textures/" << name;

  auto model = m_renderer->create_texture_pointer(slot, texture_file_path.str());
  if (model.is_err()) {
    return std::move(result::Result<std::size_t, error::Win32Error>::Err(std::move(model).unwrap_err()));
  }
  m_texture_names.insert(std::make_pair(name, m_textures.size()));
  m_textures.emplace_back(std::move(model).unwrap());
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(m_textures.size() - 1));
}

auto
project1::manager::TextureManager::texture(std::size_t index) & noexcept
  -> project1::result::Result<project1::graphics::Texture*, std::nullptr_t>
{
  if (index >= m_textures.size()) {
    return std::move(result::Result<graphics::Texture*, std::nullptr_t>::Err(nullptr));
  }
  return std::move(result::Result<graphics::Texture*, std::nullptr_t>::Ok(m_textures[index].get()));
}

auto
project1::manager::TextureManager::texture(const std::wstring& name, UINT slot) & noexcept
  -> project1::result::Result<project1::graphics::Texture*, project1::error::Win32Error>
{
  auto it = m_texture_names.find(name);
  if (it == m_texture_names.end()) {
    if (auto result = load(name, slot); result.is_err()) {
      return std::move(result::Result<graphics::Texture*, error::Win32Error>::Err(std::move(result).unwrap_err()));
    } else {
      return std::move(
	result::Result<graphics::Texture*, error::Win32Error>::Ok(m_textures[std::move(result).unwrap()].get()));
    }
  }
  return std::move(result::Result<graphics::Texture*, error::Win32Error>::Ok(m_textures[it->second].get()));
}
