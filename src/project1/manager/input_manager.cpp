#include "project1/manager/input_manager.hpp"

#include <iostream>

project1::manager::InputManager::InputManager(bool controller) noexcept
  : m_devices{ &m_keyboard, &m_mouse }
{}

auto
project1::manager::InputManager::action(std::wstring const& name) const& noexcept
  -> project1::option::Option<project1::input::Action const*>
{
  auto it = m_action_names.find(name);
  if (it == m_action_names.end()) {
    return std::move(option::Option<project1::input::Action const*>::None());
  }
  return std::move(option::Option<project1::input::Action const*>::Some(&m_actions[it->second]));
}
auto
project1::manager::InputManager::action(std::size_t index) const& noexcept
  -> project1::option::Option<project1::input::Action const*>
{
  if (index >= m_actions.size()) {
    return std::move(option::Option<project1::input::Action const*>::None());
  }
  return std::move(option::Option<project1::input::Action const*>::Some(&m_actions[index]));
}
auto
project1::manager::InputManager::action_index(std::wstring const& name) const& noexcept
  -> project1::option::Option<size_t>
{
  auto it = m_action_names.find(name);
  if (it == m_action_names.end()) {
    return std::move(option::Option<std::size_t>::None());
  }
  return std::move(option::Option<std::size_t>::Some(it->second));
}

auto
project1::manager::InputManager::add_action(project1::input::Action&& action, std::wstring_view name) & noexcept -> void
{
  m_action_names.emplace(name, m_actions.size());
  m_actions.emplace_back(std::forward<input::Action>(action));
}

auto
project1::manager::InputManager::add_axis(project1::input::Axis&& axis, std::wstring_view name) & noexcept -> void
{
  m_axis_names.emplace(name, m_axes.size());
  m_axes.emplace_back(std::forward<input::Axis>(axis));
}

auto
project1::manager::InputManager::axis(const std::wstring& name) const& noexcept
  -> project1::option::Option<project1::input::Axis const*>
{
  auto it = m_axis_names.find(name);
  if (it == m_axis_names.end()) {
    return std::move(option::Option<project1::input::Axis const*>::None());
  }
  return std::move(option::Option<project1::input::Axis const*>::Some(&m_axes[it->second]));
}

auto
project1::manager::InputManager::axis(std::size_t index) const& noexcept
  -> project1::option::Option<project1::input::Axis const*>
{
  if (index >= m_axes.size()) {
    return std::move(option::Option<project1::input::Axis const*>::None());
  }
  return std::move(option::Option<project1::input::Axis const*>::Some(&m_axes[index]));
}

auto
project1::manager::InputManager::axis_index(const std::wstring& name) const& noexcept
  -> project1::option::Option<size_t>
{
  auto it = m_axis_names.find(name);
  if (it == m_axis_names.end()) {
    return std::move(option::Option<std::size_t>::None());
  }
  return std::move(option::Option<std::size_t>::Some(it->second));
}

auto
project1::manager::InputManager::begin_update() & noexcept -> void
{
  for (auto* device : m_devices) {
    device->begin_update();

    while (device->has_event()) {
      process_event(device->pop_event());
    }
  }

  for (auto& action : m_actions) {
    action.begin_update();
  }

  for (auto& axis : m_axes) {
    axis.begin_update();
  }
}

auto
project1::manager::InputManager::end_update() & noexcept -> void
{
  for (auto* device : m_devices) {
    device->end_update();
  }

  for (auto& action : m_actions) {
    action.end_update();
  }

  for (auto& axis : m_axes) {
    axis.end_update();
  }
}

auto
project1::manager::InputManager::new_pointer() noexcept -> project1::manager::InputManager*
{
  return new InputManager(false);
}

auto
project1::manager::InputManager::process_event(project1::input::Event const& event) & noexcept -> void
{
  for (auto& action : m_actions) {
    action.process_event(event);
  }

  for (auto& axis : m_axes) {
    axis.process_event(event);
  }
}
