#include "project1/manager/model_manager.hpp"

#include <iostream>
#include <sstream>

template<>
std::unique_ptr<project1::manager::ModelManager>
  project1::utility::Singleton<project1::manager::ModelManager>::m_instance = nullptr;

template<>
std::mutex project1::utility::Singleton<project1::manager::ModelManager>::m_instance_mutex = std::mutex();

project1::manager::ModelManager::ModelManager(graphics::Renderer* renderer) noexcept
  : project1::utility::Singleton<project1::manager::ModelManager>()
  , m_renderer(renderer)
{
  using namespace std::literals;
  auto cube = m_renderer->create_cube_pointer();
  if (cube.is_err()) {
    std::wcerr << std::move(cube).unwrap_err() << std::endl;
    return;
  }
  m_model_names.insert(std::make_pair(L"Cube"s, m_models.size()));
  m_models.emplace_back(std::move(cube).unwrap());
}

auto
project1::manager::ModelManager::index(std::wstring const& name) & noexcept
  -> project1::result::Result<std::size_t, project1::error::Win32Error>
{
  auto it = m_model_names.find(name);
  if (it == m_model_names.end()) {
    return std::move(load(name));
  }
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(it->second));
}

auto
project1::manager::ModelManager::load(std::wstring const& name) & noexcept
  -> project1::result::Result<std::size_t, project1::error::Win32Error>
{
  auto mesh_file_path = std::wostringstream();
  mesh_file_path << L"data/models/" << name;

  auto model = m_renderer->load_triangle_mesh_obj_pointer(mesh_file_path.str());
  if (model.is_err()) {
    return std::move(result::Result<std::size_t, error::Win32Error>::Err(std::move(model).unwrap_err()));
  }
  m_model_names.insert(std::make_pair(name, m_models.size()));
  m_models.emplace_back(std::move(model).unwrap());
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(m_models.size() - 1));
}

auto
project1::manager::ModelManager::model(std::size_t index) & noexcept
  -> project1::result::Result<project1::graphics::Drawable*, std::nullptr_t>
{
  if (index >= m_models.size()) {
    return std::move(result::Result<graphics::Drawable*, std::nullptr_t>::Err(nullptr));
  }
  return std::move(result::Result<graphics::Drawable*, std::nullptr_t>::Ok(m_models[index].get()));
}

auto
project1::manager::ModelManager::model(const std::wstring& name) & noexcept
  -> project1::result::Result<project1::graphics::Drawable*, project1::error::Win32Error>
{
  auto it = m_model_names.find(name);
  if (it == m_model_names.end()) {
    if (auto result = load(name); result.is_err()) {
      return std::move(result::Result<graphics::Drawable*, error::Win32Error>::Err(std::move(result).unwrap_err()));
    } else {
      return std::move(
	result::Result<graphics::Drawable*, error::Win32Error>::Ok(m_models[std::move(result).unwrap()].get()));
    }
  }
  return std::move(result::Result<graphics::Drawable*, error::Win32Error>::Ok(m_models[it->second].get()));
}
