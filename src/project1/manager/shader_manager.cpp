#include "project1/manager/shader_manager.hpp"

#include <sstream>

template<>
std::unique_ptr<project1::manager::ShaderManager>
  project1::utility::Singleton<project1::manager::ShaderManager>::m_instance = nullptr;

template<>
std::mutex project1::utility::Singleton<project1::manager::ShaderManager>::m_instance_mutex = std::mutex();

project1::manager::ShaderManager::ShaderManager(graphics::Renderer* renderer) noexcept
  : project1::utility::Singleton<project1::manager::ShaderManager>()
  , m_renderer(renderer)
{}

auto
project1::manager::ShaderManager::index(const std::wstring& name) & noexcept
  -> project1::result::Result<std::size_t, project1::error::Win32Error>
{
  auto it = m_shader_names.find(name);
  if (it == m_shader_names.end()) {
    return std::move(load(name));
  }
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(it->second));
}

auto
project1::manager::ShaderManager::load(const std::wstring& name) & noexcept
  -> project1::result::Result<size_t, project1::error::Win32Error>
{
  auto pixel_shader_file_path = std::wostringstream();
  pixel_shader_file_path << L"data/shaders/" << name << L".ps.hlsl";
  auto vertex_shader_file_path = std::wostringstream();
  vertex_shader_file_path << L"data/shaders/" << name << L".vs.hlsl";

  auto shader = m_renderer->create_shader_pointer(vertex_shader_file_path.str(), pixel_shader_file_path.str());
  if (shader.is_err()) {
    return std::move(result::Result<std::size_t, error::Win32Error>::Err(std::move(shader).unwrap_err()));
  }
  m_shader_names.insert(std::make_pair(name, m_shaders.size()));
  m_shaders.emplace_back(std::move(shader).unwrap());
  return std::move(result::Result<std::size_t, error::Win32Error>::Ok(m_shaders.size() - 1));
}

auto
project1::manager::ShaderManager::shader(std::size_t index) & noexcept
  -> project1::result::Result<project1::graphics::Shader*, std::nullptr_t>
{
  if (index >= m_shaders.size()) {
    return std::move(result::Result<graphics::Shader*, std::nullptr_t>::Err(nullptr));
  }
  return std::move(result::Result<graphics::Shader*, std::nullptr_t>::Ok(m_shaders[index].get()));
}

auto
project1::manager::ShaderManager::shader(std::wstring const& name) & noexcept
  -> project1::result::Result<project1::graphics::Shader*, project1::error::Win32Error>
{
  auto it = m_shader_names.find(name);
  if (it == m_shader_names.end()) {
    if (auto result = load(name); result.is_err()) {
      return std::move(result::Result<graphics::Shader*, error::Win32Error>::Err(std::move(result).unwrap_err()));
    } else {
      return std::move(
	result::Result<graphics::Shader*, error::Win32Error>::Ok(m_shaders[std::move(result).unwrap()].get()));
    }
  }
  return std::move(result::Result<graphics::Shader*, error::Win32Error>::Ok(m_shaders[it->second].get()));
}
