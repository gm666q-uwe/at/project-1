#include "project1/input/device.hpp"

project1::input::Device::Device(project1::input::Device::Type type) noexcept
  : m_type(type)
{}

auto
project1::input::Device::has_event() const& noexcept -> bool
{
  return !m_events.empty();
}

auto
project1::input::Device::pop_event() & noexcept -> project1::input::Event
{
  auto event = m_events.front();
  m_events.pop();
  return std::move(event);
}
