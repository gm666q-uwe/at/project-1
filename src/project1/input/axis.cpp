#include "project1/input/axis.hpp"

project1::input::Axis::Axis(std::vector<Binding>&& bindings) noexcept
  : m_bindings(std::forward<std::vector<Binding>>(bindings))
{}

auto
project1::input::Axis::begin_update() & noexcept -> void
{
  m_value = 0.0f;

  for (auto& binding : m_bindings) {
    m_value += binding.scale * binding.value;
  }
}

auto
project1::input::Axis::end_update() & noexcept -> void
{}

auto
project1::input::Axis::new_(std::vector<Binding>&& bindings) noexcept -> project1::input::Axis
{
  return std::move(Axis(std::forward<std::vector<Binding>>(bindings)));
}

auto
project1::input::Axis::new_pointer(std::vector<Binding>&& bindings) noexcept -> project1::input::Axis*
{
  return new Axis(std::forward<std::vector<Binding>>(bindings));
}

auto
project1::input::Axis::process_event(project1::input::Event const& event) & noexcept -> void
{
  for (auto& binding : m_bindings) {
    if (binding.type == event.type && binding.number == event.number) {
      binding.value = event.value;
    }
  }
}
