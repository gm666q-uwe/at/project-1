#include "project1/input/action.hpp"

project1::input::Action::Action(std::vector<Binding>&& bindings) noexcept
  : m_bindings(std::forward<std::vector<Binding>>(bindings))
{}

auto
project1::input::Action::begin_update() & noexcept -> void
{
  m_previous_value = false;
  m_value = false;

  for (auto& binding : m_bindings) {
    if (binding.previous_value) {
      m_previous_value = true;
    }
    if (binding.value) {
      m_value = true;
    }
  }
}

auto
project1::input::Action::end_update() & noexcept -> void
{
  for (auto& binding : m_bindings) {
    binding.previous_value = binding.value;
  }
}

auto
project1::input::Action::new_(std::vector<Binding>&& bindings) noexcept -> project1::input::Action
{
  return std::move(Action(std::forward<std::vector<Binding>>(bindings)));
}

auto
project1::input::Action::new_pointer(std::vector<Binding>&& bindings) noexcept -> project1::input::Action*
{
  return new Action(std::forward<std::vector<Binding>>(bindings));
}

auto
project1::input::Action::process_event(project1::input::Event const& event) & noexcept -> void
{
  for (auto& binding : m_bindings) {
    if (binding.type == event.type && binding.number == event.number) {
      binding.value = binding.dead_zone < 0.0f ? event.value < binding.dead_zone : event.value > binding.dead_zone;
    }
  }
}
