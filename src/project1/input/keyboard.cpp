#include "project1/input/keyboard.hpp"

project1::input::Keyboard::Keyboard() noexcept
  : project1::input::Device(project1::input::Device::Type::Keyboard)
{}

auto
project1::input::Keyboard::begin_update() & noexcept -> void
{}

auto
project1::input::Keyboard::characters() const& noexcept -> std::wstring_view
{
  return m_characters;
}

auto
project1::input::Keyboard::end_update() & noexcept -> void
{
  m_characters.clear();
}

auto
project1::input::Keyboard::new_() noexcept -> project1::input::Keyboard
{
  return std::move(Keyboard());
}

auto
project1::input::Keyboard::new_pointer() noexcept -> project1::input::Keyboard*
{
  return new Keyboard();
}

auto
project1::input::Keyboard::process_char(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_characters.push_back(w_parameter);
}

auto
project1::input::Keyboard::process_key(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_events.emplace(w_parameter, Event::Type::Key, ((l_parameter >> 16) & KF_UP) == KF_UP ? 0.0f : 1.0f);
}
