#include "project1/input/mouse.hpp"

#include <iostream>

project1::input::Mouse::Mouse() noexcept
  : project1::input::Device(project1::input::Device::Type::Mouse)
{}

void
project1::input::Mouse::begin_update() & noexcept
{
  auto relative = POINT{ m_position.x - m_previous_position.x, m_position.y - m_previous_position.y };
  m_events.emplace(0, Event::Type::RelativeAxis, static_cast<float>(relative.x));
  m_events.emplace(1, Event::Type::RelativeAxis, static_cast<float>(-relative.y));
}

void
project1::input::Mouse::end_update() & noexcept
{
  m_previous_position = m_position;
}

auto
project1::input::Mouse::new_() noexcept -> project1::input::Mouse
{
  return std::move(Mouse());
}

auto
project1::input::Mouse::new_pointer() noexcept -> project1::input::Mouse*
{
  return new Mouse();
}

auto
project1::input::Mouse::process_left_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_events.emplace(VK_LBUTTON, Event::Type::Key, (w_parameter & MK_LBUTTON) == MK_LBUTTON ? 1.0f : 0.0f);
}

auto
project1::input::Mouse::process_middle_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_events.emplace(VK_MBUTTON, Event::Type::Key, (w_parameter & MK_MBUTTON) == MK_MBUTTON ? 1.0f : 0.0f);
}

auto
project1::input::Mouse::process_move(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_position = MAKEPOINTS(l_parameter);
  m_events.emplace(0, Event::Type::AbsoluteAxis, static_cast<float>(m_position.x));
  m_events.emplace(1, Event::Type::AbsoluteAxis, static_cast<float>(m_position.y));
}

auto
project1::input::Mouse::process_right_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  m_events.emplace(VK_RBUTTON, Event::Type::Key, (w_parameter & MK_RBUTTON) == MK_RBUTTON ? 1.0f : 0.0f);
}

auto
project1::input::Mouse::process_x_button(WPARAM w_parameter, LPARAM l_parameter) & noexcept -> void
{
  switch (GET_XBUTTON_WPARAM(w_parameter)) {
    case XBUTTON1:
      m_events.emplace(VK_XBUTTON1, Event::Type::Key, (w_parameter & MK_XBUTTON1) == MK_XBUTTON1 ? 1.0f : 0.0f);
      break;
    case XBUTTON2:
      m_events.emplace(VK_XBUTTON2, Event::Type::Key, (w_parameter & MK_XBUTTON2) == MK_XBUTTON2 ? 1.0f : 0.0f);
      break;
    default:
      return;
  }
}
