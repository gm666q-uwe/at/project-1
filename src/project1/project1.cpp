#include "project1/project1.hpp"

#include <DirectXMath/DirectXColors.h>

#include <iostream>

#include "project1/manager/model_manager.hpp"
#include "project1/manager/shader_manager.hpp"
#include "project1/manager/texture_manager.hpp"

project1::Project1::~Project1() noexcept
{
  m_transform_buffer.reset();
  m_renderer = nullptr;
  m_window.reset();
  m_display.reset();
}

auto
project1::Project1::fixed_update(double fixed_delta_time, double fixed_time) & noexcept -> void
{}

auto
project1::Project1::init(std::span<std::wstring_view> args) & noexcept -> void
{
  using namespace std::literals;

  using C = ecs::ComponentList<int>;
  using S = ecs::SignatureList<int>;
  using T = ecs::TagList<int>;

  using s = ecs::Settings<C, S, T>;
  std::wcout << std::boolalpha << s::component_count() << std::endl
	     << s::component_bit<int>() << std::endl
	     << s::component_id<int>() << std::endl
	     << s::is_component<int>() << std::endl
	     << s::is_signature<int>() << std::endl
	     << s::is_tag<int>() << std::endl
	     << s::signature_count() << std::endl
	     << s::signature_id<int>() << std::endl
	     << s::tag_bit<int>() << std::endl
	     << s::tag_count() << std::endl
	     << s::tag_id<int>() << std::endl;

  {
    auto display = display::Display::new_pointer(m_instance);
    if (display.is_err()) {
      std::wcerr << std::move(display).unwrap_err() << std::endl;
      m_result = -1;
      m_running = false;
      return;
    }
    m_display = std::unique_ptr<display::Display>(std::move(display).unwrap());
  }

  {
    auto window = m_display->create_window_pointer(800, 600, L"Project1"sv, m_show_command);
    if (window.is_err()) {
      std::wcerr << std::move(window).unwrap_err() << std::endl;
      m_result = -2;
      m_running = false;
      return;
    }
    m_window = std::unique_ptr<display::Window>(std::move(window).unwrap());
    auto* input_manager = m_window->input_manager();
    input_manager->add_axis(input::Axis::new_({ input::Axis::Binding(0x41, -1.0f, input::Event::Type::Key),
						input::Axis::Binding(0x44, 1.0f, input::Event::Type::Key) }),
			    L"X Axis"sv);
    input_manager->add_axis(input::Axis::new_({ input::Axis::Binding(0x53, -1.0f, input::Event::Type::Key),
						input::Axis::Binding(0x57, 1.0f, input::Event::Type::Key) }),
			    L"Y Axis"sv);
  }

  {
    auto renderer = m_window->renderer();
    if (renderer.is_err()) {
      std::wcerr << std::move(renderer).unwrap_err() << std::endl;
      m_result = -3;
      m_running = false;
      return;
    }
    m_renderer = std::move(renderer).unwrap();
  }

  manager::ModelManager::instance(m_renderer).model(L"bunny.obj"s).unwrap();
  manager::ShaderManager::instance(m_renderer).shader(L"Main"s).unwrap()->bind();
  if (auto texture = manager::TextureManager::instance(m_renderer).texture(L"cube.png"s); texture.is_err()) {
    std::wcout << "ERROR: " << std::move(texture).unwrap_err() << std::endl;
  } else {
    std::move(texture).unwrap()->bind();
  }

  {
    auto geometry_matrix = DirectX::XMMatrixRotationRollPitchYaw(0.0f, DirectX::XM_PI, 0.0f);
    auto view_matrix = DirectX::XMMatrixLookAtLH(DirectX::XMVECTORF32{ 0.0f, 2.5f, -5.0f, 0.0f },
						 DirectX::XMVECTORF32{ 0.0f, 0.0f, 0.0f, 0.0f },
						 DirectX::XMVECTORF32{ 0.0f, 1.0f, 0.0f, 0.0f });

    auto transform = graphics::Transform{};
    DirectX::XMStoreFloat4x4A(&transform.view_projection,
			      DirectX::XMMatrixTranspose(view_matrix * m_renderer->perspective_matrix()));
    DirectX::XMStoreFloat4x4A(&transform.geometry, DirectX::XMMatrixTranspose(geometry_matrix));

    auto transform_buffer =
      m_renderer->create_constant_buffer_with_data_pointer<graphics::Transform, graphics::Shader::Type::Vertex>(
	transform, 0);
    if (transform_buffer.is_err()) {
      std::wcerr << std::move(transform_buffer).unwrap_err() << std::endl;
      m_result = -5;
      m_running = false;
      return;
    }
    m_transform_buffer = std::unique_ptr<graphics::TransformBuffer<>>(std::move(transform_buffer).unwrap());
    m_transform_buffer->bind();
  }
}

auto
project1::Project1::input() & noexcept -> void
{
  auto* input_manager = m_window->input_manager();
  input_manager->end_update();
  m_running = m_display->run();
  if (m_running) {
    m_running = m_window->run();
    if (m_running) {
      input_manager->begin_update();
    }
  }
}

auto
project1::Project1::late_update(double delta_time, double time) & noexcept -> void
{}

auto
project1::Project1::new_() noexcept -> project1::Project1
{
  return Project1();
}

auto
project1::Project1::new_pointer() noexcept -> project1::Project1*
{
  return new Project1();
}

auto
project1::Project1::render(double time) & noexcept -> void
{
  using namespace std::literals;

  m_renderer->begin_frame();
  m_renderer->clear(DirectX::Colors::Black);

  manager::ModelManager::instance()->model(L"bunny.obj"s).unwrap()->draw();
  manager::ModelManager::instance()->model(L"Cube"s).unwrap()->draw();

  m_renderer->end_frame();
}

auto
project1::Project1::result() const& noexcept -> int
{
  return m_result;
}

auto
project1::Project1::run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd) & noexcept -> int
{
  m_instance = hInstance;
  m_show_command = nShowCmd;
  return App::run(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
}

auto
project1::Project1::running() const& noexcept -> bool
{
  return m_running;
}

auto
project1::Project1::update(double delta_time, double time) & noexcept -> void
{}
